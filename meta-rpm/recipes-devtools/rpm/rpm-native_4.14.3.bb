include rpm-native.inc

SRC_URI = "http://ftp.rpm.org/releases/rpm-4.14.x/rpm-4.14.3.tar.bz2 \
           file://environment.d-rpm.sh \
           file://rpm-4.14.2-no-short-circuit.patch \
           file://0001-Do-not-read-config-files-from-HOME.patch \
           file://0002-Add-support-for-prefixing-etc-from-RPM_ETCCONFIGDIR-.patch \
           file://0011-Do-not-require-that-ELF-binaries-are-executable-to-b.patch \
           file://0001-perl-disable-auto-reqs.patch \
           file://0001-rpm-rpmio.c-restrict-virtual-memory-usage-if-limit-s.patch \
           file://0016-rpmscript.c-change-logging-level-around-scriptlets-t.patch \
           file://0001-rpmplugins.c-call-dlerror-prior-to-dlsym.patch \
           file://Unset-D-in-scriptlets.patch \
           "

SRC_URI[sha256sum] = "13711268181a6e201eb9d4eb06384a7fcc42dcc6c9057a62c53472cfc5ba44b5"
LIC_FILES_CHKSUM = "file://COPYING;md5=c4eec0c20c6034b9407a09945b48a43f"

S = "${WORKDIR}/rpm-${PV}"
