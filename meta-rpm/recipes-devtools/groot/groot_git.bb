require groot.inc

SRC_URI = "git://gitlab.com/fedora-iot/groot.git;branch=main;protocol=https"

SRCREV = "ebda9e61055f9d46be0d444853d08c11040a1d78"

S = "${WORKDIR}/git"
PV = "0.1+git${SRCPV}"
