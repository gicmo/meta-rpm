SUMMARY = "generated recipe based on perl-Test-Harness srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Harness = "perl-Carp perl-Exporter perl-File-Path perl-Getopt-Long perl-IO perl-PathTools perl-Text-ParseWords perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Test-Harness-3.42-1.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Harness.sha256sum] = "566cbfbec09e18e243b0f3cb17439d020cc52ae7dd09ecb6df7dce4a897ff445"
