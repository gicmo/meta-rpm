SUMMARY = "generated recipe based on xorg-x11-docs srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-docs-1.7.1-7.el8.noarch.rpm \
          "

SRC_URI[xorg-x11-docs.sha256sum] = "f706026925c7f4b7348017cc575f58fa7636bace90c026c92b67e5acd405d1cf"
