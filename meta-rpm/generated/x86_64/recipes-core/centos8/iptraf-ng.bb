SUMMARY = "generated recipe based on iptraf-ng srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_iptraf-ng = "libc.so.6 libncursesw.so.6 libpanel.so.6 libtinfo.so.6"
RDEPENDS_iptraf-ng = "glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptraf-ng-1.1.4-18.el8.x86_64.rpm \
          "

SRC_URI[iptraf-ng.sha256sum] = "aaf6a1fd1c40da8316078c99b70f4550545382e3257b2a9a1bf3259f69e8412c"
