SUMMARY = "generated recipe based on words srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/words-3.0-28.el8.noarch.rpm \
          "

SRC_URI[words.sha256sum] = "d1bbc1d1445f84d55f7c641f6b996eb0383b52ce08a96c3a58bc9990a32178fa"
