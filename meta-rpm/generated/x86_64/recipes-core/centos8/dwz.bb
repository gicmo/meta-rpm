SUMMARY = "generated recipe based on dwz srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-3.0"
RPM_LICENSE = "GPLv2+ and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_dwz = "libc.so.6 libelf.so.1"
RDEPENDS_dwz = "elfutils-libelf glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dwz-0.12-9.el8.x86_64.rpm \
          "

SRC_URI[dwz.sha256sum] = "37ddd4817ea8ec29c9969a7d8402bfd75d51ca8350f43d6ac80778d730e84a54"
