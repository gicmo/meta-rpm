SUMMARY = "generated recipe based on libpst srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpst-libs = "libpst.so.4"
RPM_SONAME_REQ_libpst-libs = "libc.so.6 libpthread.so.0"
RDEPENDS_libpst-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpst-libs-0.6.71-8.el8.x86_64.rpm \
          "

SRC_URI[libpst-libs.sha256sum] = "a8dd98011590535cad33f0779eb200c8a399fa1aaff25e37447960d9748f9729"
