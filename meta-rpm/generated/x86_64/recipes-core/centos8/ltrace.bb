SUMMARY = "generated recipe based on ltrace srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "elfutils libselinux pkgconfig-native"
RPM_SONAME_REQ_ltrace = "libc.so.6 libdw.so.1 libelf.so.1 libselinux.so.1 libstdc++.so.6"
RDEPENDS_ltrace = "elfutils-libelf elfutils-libs glibc libselinux libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ltrace-0.7.91-28.el8.x86_64.rpm \
          "

SRC_URI[ltrace.sha256sum] = "965661458677c0cb48eafa31d105c66276e538ed0f04098150e71d80d1184f4f"
