SUMMARY = "generated recipe based on perl-Getopt-Long srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPLv2+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Getopt-Long = "perl-Exporter perl-Pod-Usage perl-Text-ParseWords perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Getopt-Long-2.50-4.el8.noarch.rpm \
          "

SRC_URI[perl-Getopt-Long.sha256sum] = "da4c6daa0d5406bc967cc89b02a69689491f42c543aceea1a31136f0f1a8d991"
