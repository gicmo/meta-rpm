SUMMARY = "generated recipe based on omping srpm"
DESCRIPTION = "Description"
LICENSE = "ISC"
RPM_LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_omping = "libc.so.6"
RDEPENDS_omping = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/omping-0.0.4-14.el8.x86_64.rpm \
          "

SRC_URI[omping.sha256sum] = "2f20068cfb309a8ce151d202cfbc35137e04b62ee030ed7e1f5b0eac44981351"
