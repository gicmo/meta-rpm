SUMMARY = "generated recipe based on perl-Unicode-UTF8 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unicode-UTF8 = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unicode-UTF8 = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Unicode-UTF8-0.62-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Unicode-UTF8.sha256sum] = "f8358a96a614ead65ecf924b24b054d841214fe3c4acb6b0290458eeada715e9"
