SUMMARY = "generated recipe based on perl-TermReadKey srpm"
DESCRIPTION = "Description"
LICENSE = "(CLOSED) & (Artistic-1.0 | GPL-2.0)"
RPM_LICENSE = "(Copyright only) and (Artistic or GPL+)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-TermReadKey = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-TermReadKey = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-TermReadKey-2.37-7.el8.x86_64.rpm \
          "

SRC_URI[perl-TermReadKey.sha256sum] = "62df66999d0074fb92b4cc78554ab7134782803d070d5629e6bfa293bcfb55a6"
