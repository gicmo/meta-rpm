SUMMARY = "generated recipe based on perl-PadWalker srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-PadWalker = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-PadWalker = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-PadWalker-2.3-2.el8.x86_64.rpm \
          "

SRC_URI[perl-PadWalker.sha256sum] = "1414c5ad97b94bef8de8370e31c0b9e1812d18c6b1575523e3d61e3257bb2e36"
