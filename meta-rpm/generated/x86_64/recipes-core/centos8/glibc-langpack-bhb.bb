SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-bhb = "locale-base-bhb-in.utf8 (= 2.28) virtual-locale-bhb (= 2.28) virtual-locale-bhb-in.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-bhb = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bhb-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-bhb.sha256sum] = "9cdfa3780ae3d2f2a2981c7bc5ef0e644946006ef45f9f3ea1b6ec5f7f15783c"
