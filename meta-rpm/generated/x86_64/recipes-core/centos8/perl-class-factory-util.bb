SUMMARY = "generated recipe based on perl-Class-Factory-Util srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Factory-Util = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Class-Factory-Util-1.7-27.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Factory-Util.sha256sum] = "38d3a3c2f2346fc92412b31d5ae2bbedf64f0a39a02df6d7e40f4dd3632fb01c"
