SUMMARY = "generated recipe based on pyparted srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "parted pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-pyparted = "libc.so.6 libparted.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pyparted = "glibc parted platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pyparted-3.11.0-13.el8.x86_64.rpm \
          "

SRC_URI[python3-pyparted.sha256sum] = "afa7f94d2463693a0824e8c8efb88969cdca6961ee40ecd8e994b71d38a10fc8"
