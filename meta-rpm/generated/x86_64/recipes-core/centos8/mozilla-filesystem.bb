SUMMARY = "generated recipe based on mozilla-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-1.1"
RPM_LICENSE = "MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mozilla-filesystem-1.9-18.el8.x86_64.rpm \
          "

SRC_URI[mozilla-filesystem.sha256sum] = "949c30f0a4812faeeb70a25519a724ab5f637b62005308a7235f200a1ae41ffe"
