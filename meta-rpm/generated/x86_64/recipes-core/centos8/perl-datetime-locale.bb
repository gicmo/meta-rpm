SUMMARY = "generated recipe based on perl-DateTime-Locale srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and Unicode"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-Locale = "perl-Carp perl-Dist-CheckConflicts perl-Exporter perl-File-ShareDir perl-Params-ValidationCompiler perl-Scalar-List-Utils perl-Specio perl-libs perl-namespace-autoclean"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-Locale-1.17-2.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-Locale.sha256sum] = "b021ad0860fd08ebf7c0afeb02753b3047064d2fa98331c4f57cb2cf68cf4f59"
