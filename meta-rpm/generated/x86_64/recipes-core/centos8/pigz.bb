SUMMARY = "generated recipe based on pigz srpm"
DESCRIPTION = "Description"
LICENSE = "Zlib"
RPM_LICENSE = "zlib"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_REQ_pigz = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_pigz = "glibc libgcc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pigz-2.4-4.el8.x86_64.rpm \
          "

SRC_URI[pigz.sha256sum] = "38f93036a50da87f65f680e9fb22db47b17bc8fffdaf19e6398b6fb28e0ab262"
