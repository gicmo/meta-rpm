SUMMARY = "generated recipe based on mythes-ne srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-ne = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-ne-1.1-14.el8.noarch.rpm \
          "

SRC_URI[mythes-ne.sha256sum] = "0745ab669e6f78ea053707261d151ef7a3ed1542898c9eaf16d45a35aa9af2a1"
