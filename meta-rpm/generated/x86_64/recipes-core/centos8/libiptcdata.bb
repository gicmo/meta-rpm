SUMMARY = "generated recipe based on libiptcdata srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libiptcdata = "libiptcdata.so.0"
RPM_SONAME_REQ_libiptcdata = "libc.so.6"
RDEPENDS_libiptcdata = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libiptcdata-1.0.4-21.el8.x86_64.rpm \
          "

SRC_URI[libiptcdata.sha256sum] = "75ae631487d545f8fd98439051b5a504f9958d5374b9ee968ef3aa61574080d0"
