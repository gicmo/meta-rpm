SUMMARY = "generated recipe based on patchutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_patchutils = "libc.so.6"
RDEPENDS_patchutils = "bash glibc perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/patchutils-0.3.4-10.el8.x86_64.rpm \
          "

SRC_URI[patchutils.sha256sum] = "052d41dd231516b2cb91026153da70d5d6f04010d515956169c40e61bb622041"
