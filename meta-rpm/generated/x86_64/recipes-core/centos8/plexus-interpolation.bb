SUMMARY = "generated recipe based on plexus-interpolation srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & CLOSED & MIT"
RPM_LICENSE = "ASL 2.0 and ASL 1.1 and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-interpolation = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_plexus-interpolation-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-interpolation-1.22-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-interpolation-javadoc-1.22-9.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-interpolation.sha256sum] = "ec858ddcf0cf78bcb418d461b810a024fe601e7639357e8cbb9b59d92388684b"
SRC_URI[plexus-interpolation-javadoc.sha256sum] = "a2135102d38f44aeec1cb3727741f0904491d91a724430b40a1d15b1b7d7c4d6"
