SUMMARY = "generated recipe based on libfabric srpm"
DESCRIPTION = "Description"
LICENSE = "BSD | GPL-2.0"
RPM_LICENSE = "BSD or GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnl libpsm2 pkgconfig-native rdma-core"
RPM_SONAME_PROV_libfabric = "libfabric.so.1"
RPM_SONAME_REQ_libfabric = "libc.so.6 libdl.so.2 libibverbs.so.1 libnl-3.so.200 libnl-route-3.so.200 libpsm2.so.2 libpthread.so.0 librdmacm.so.1 librt.so.1"
RDEPENDS_libfabric = "glibc libibverbs libnl3 libpsm2 librdmacm"
RPM_SONAME_REQ_libfabric-devel = "libfabric.so.1"
RPROVIDES_libfabric-devel = "libfabric-dev (= 1.9.0rc1)"
RDEPENDS_libfabric-devel = "libfabric pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libfabric-1.9.0rc1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libfabric-devel-1.9.0rc1-1.el8.x86_64.rpm \
          "

SRC_URI[libfabric.sha256sum] = "e868efb850c961a2ee2220d92f4df7f604d134013bab3ba9c04a2e219750bee5"
SRC_URI[libfabric-devel.sha256sum] = "7312c4b53e445e825e31ae90b5e242c14d638faa2173acd32bf4d28d38883c05"
