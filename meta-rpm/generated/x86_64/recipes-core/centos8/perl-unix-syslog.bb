SUMMARY = "generated recipe based on perl-Unix-Syslog srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unix-Syslog = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unix-Syslog = "glibc perl-Exporter perl-interpreter perl-libs rsyslog"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Unix-Syslog-1.1-29.el8.x86_64.rpm \
          "

SRC_URI[perl-Unix-Syslog.sha256sum] = "ade931a6fed90448b09186e0483d0ac74c31b16723d4f280f635240626da2540"
