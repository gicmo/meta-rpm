SUMMARY = "generated recipe based on mlocate srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mlocate = "libc.so.6"
RDEPENDS_mlocate = "bash glibc grep sed shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mlocate-0.26-20.el8.x86_64.rpm \
          "

SRC_URI[mlocate.sha256sum] = "902cdfbf959cb68d0f1b7a6630a2cc7efa697dbe2ee5eec32b0a6ee21a90dec9"
