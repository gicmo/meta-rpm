SUMMARY = "generated recipe based on gnome-tweaks srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & CC0-1.0"
RPM_LICENSE = "GPLv3 and CC0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_gnome-tweaks = "gnome-desktop3 gnome-settings-daemon gnome-shell gnome-shell-extension-user-theme gobject-introspection gsettings-desktop-schemas gtk3 libnotify libsoup mutter pango platform-python python3-gobject-base"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-tweaks-3.28.1-7.el8.noarch.rpm \
          "

SRC_URI[gnome-tweaks.sha256sum] = "7054696a4ae8cf185abcf266764d2a3f94d02c117b7e9237682a03d757b1617e"
