SUMMARY = "generated recipe based on grafana-pcp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_grafana-pcp = "grafana"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-pcp-1.0.5-3.el8.noarch.rpm \
          "

SRC_URI[grafana-pcp.sha256sum] = "0037aa9511284622e81132ec94996ba967911b99c406be1965b98217e3daf37a"
