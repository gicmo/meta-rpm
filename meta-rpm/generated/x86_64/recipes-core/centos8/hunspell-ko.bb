SUMMARY = "generated recipe based on hunspell-ko srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-1.1 | GPL-2.0 | LGPL-2.0"
RPM_LICENSE = "MPLv1.1 or GPLv2 or LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ko = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ko-0.7.0-5.el8.noarch.rpm \
          "

SRC_URI[hunspell-ko.sha256sum] = "df8dc4511146ce60564c5e321b0a74e01e791b69ce8ef51fb8f97a1cfe113d2c"
