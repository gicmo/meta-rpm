SUMMARY = "generated recipe based on hunspell-mr srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mr = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mr-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-mr.sha256sum] = "48ca735bd4dca162a652b3db653e04576eaa32551e0a8e8890e03517173f5395"
