SUMMARY = "generated recipe based on fapolicyd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "file libcap-ng libgcrypt libseccomp lmdb pkgconfig-native rpm systemd-libs"
RPM_SONAME_REQ_fapolicyd = "libc.so.6 libcap-ng.so.0 libgcrypt.so.20 liblmdb.so.0.0.0 libmagic.so.1 libpthread.so.0 librpm.so.8 librpmio.so.8 libseccomp.so.2 libudev.so.1"
RDEPENDS_fapolicyd = "bash file-libs glibc libcap-ng libgcrypt libseccomp lmdb-libs platform-python rpm-libs shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fapolicyd-0.9.1-4.el8.x86_64.rpm \
          "

SRC_URI[fapolicyd.sha256sum] = "1de417324497c3602c677540191163d967fc00a6295d23c65e2e1f70da50f511"
