SUMMARY = "generated recipe based on rubygem-diff-lcs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0 | MIT"
RPM_LICENSE = "GPLv2+ or Artistic or MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-diff-lcs = "ruby rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rubygem-diff-lcs-1.3-4.el8.noarch.rpm \
          "

SRC_URI[rubygem-diff-lcs.sha256sum] = "e92aae92b214f425199a0cced41e4b583afe40261496590399e94985eb501580"
