SUMMARY = "generated recipe based on lua-json srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_lua-json = "lua lua-lpeg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lua-json-1.3.2-9.el8.noarch.rpm \
          "

SRC_URI[lua-json.sha256sum] = "173953721e8387d708b84fef1184d2aee2094cb98a311353ea87abdf1b341829"
