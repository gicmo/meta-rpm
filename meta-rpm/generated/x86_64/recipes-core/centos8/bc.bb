SUMMARY = "generated recipe based on bc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_bc = "libc.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_bc = "bash glibc info ncurses-libs readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bc-1.07.1-5.el8.x86_64.rpm \
          "

SRC_URI[bc.sha256sum] = "68035e01e221d5297f2fdebcbb29533787b1442eb835b37d9ccfdcc9352d954f"
