SUMMARY = "generated recipe based on libndp srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libndp = "libndp.so.0"
RPM_SONAME_REQ_libndp = "libc.so.6"
RDEPENDS_libndp = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libndp-1.7-3.el8.x86_64.rpm \
          "

SRC_URI[libndp.sha256sum] = "c357a350aa169402db3c898c7edcfee333e1d11a44f62bbeaba3fd3d2f31644d"
