SUMMARY = "generated recipe based on radvd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "BSD with advertising"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_radvd = "libc.so.6"
RDEPENDS_radvd = "bash glibc shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/radvd-2.17-14.el8.x86_64.rpm \
          "

SRC_URI[radvd.sha256sum] = "bb4bb2faf0b37cd65b955821b116d22102a13143d7a3b8493746df66793f3070"
