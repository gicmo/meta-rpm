SUMMARY = "generated recipe based on perl-Specio srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Specio = "perl-Carp perl-Devel-StackTrace perl-Eval-Closure perl-Exporter perl-MRO-Compat perl-Module-Runtime perl-Ref-Util perl-Role-Tiny perl-Scalar-List-Utils perl-Storable perl-interpreter perl-libs perl-parent perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Specio-0.42-2.el8.noarch.rpm \
          "

SRC_URI[perl-Specio.sha256sum] = "fde93c93f91ee46f9233ac887b805c95015a32ff46c84d880bea5d85698dae94"
