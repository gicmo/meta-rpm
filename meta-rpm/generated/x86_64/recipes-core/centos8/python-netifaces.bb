SUMMARY = "generated recipe based on python-netifaces srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-netifaces = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-netifaces = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-netifaces-0.10.6-4.el8.x86_64.rpm \
          "

SRC_URI[python3-netifaces.sha256sum] = "c45eaea64b54b27bcacb6d8e542c72ddbed5fafcd5e2facb08f7c04775e4f381"
