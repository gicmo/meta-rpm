SUMMARY = "generated recipe based on pytest srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pytest = "platform-python platform-python-setuptools python3-attrs python3-pluggy python3-py python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pytest-3.4.2-11.el8.noarch.rpm \
          "

SRC_URI[python3-pytest.sha256sum] = "a732fc5a9174d3d0274e7c05e53d5cd04ebf8d3dc684c0f7f6b613f1d9bf8920"
