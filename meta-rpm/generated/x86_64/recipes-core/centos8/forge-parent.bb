SUMMARY = "generated recipe based on forge-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_forge-parent = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/forge-parent-38-11.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[forge-parent.sha256sum] = "90e6792e063760c01b03bdd58430c68cc32a57ebe6a1246dbed2682e0ab22845"
