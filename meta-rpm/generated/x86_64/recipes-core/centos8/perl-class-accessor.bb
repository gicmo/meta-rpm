SUMMARY = "generated recipe based on perl-Class-Accessor srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Accessor = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Class-Accessor-0.51-2.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Accessor.sha256sum] = "221bc9e311849a150e8dbede20210dfbce21d82282160adb8b2ba531be3decb1"
