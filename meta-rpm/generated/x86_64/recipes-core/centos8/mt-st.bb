SUMMARY = "generated recipe based on mt-st srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mt-st = "libc.so.6"
RDEPENDS_mt-st = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mt-st-1.1-24.el8.x86_64.rpm \
          "

SRC_URI[mt-st.sha256sum] = "8d20460c2a9b9b36a2836f05694f724271ed4d223e0e161930cc5501d38711e1"
