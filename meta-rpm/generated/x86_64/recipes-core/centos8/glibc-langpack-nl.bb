SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-nl = "locale-base-nl-aw (= 2.28) locale-base-nl-be (= 2.28) locale-base-nl-be.utf8 (= 2.28) locale-base-nl-be@euro (= 2.28) locale-base-nl-nl (= 2.28) locale-base-nl-nl.utf8 (= 2.28) locale-base-nl-nl@euro (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl-aw (= 2.28) virtual-locale-nl-be (= 2.28) virtual-locale-nl-be.utf8 (= 2.28) virtual-locale-nl-be@euro (= 2.28) virtual-locale-nl-nl (= 2.28) virtual-locale-nl-nl.utf8 (= 2.28) virtual-locale-nl-nl@euro (= 2.28)"
RDEPENDS_glibc-langpack-nl = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nl-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-nl.sha256sum] = "160931f82cae4171c186cff0d80c628c2d3d0bc9ad9bcc826d4f4102d08562fe"
