SUMMARY = "generated recipe based on perl-Fedora-VSP srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Fedora-VSP = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Fedora-VSP-0.001-9.el8.noarch.rpm \
          "

SRC_URI[perl-Fedora-VSP.sha256sum] = "021176d87c8897bc948e11230fd05b0c12af1037d2aee41c3b8c4fcea7bb9089"
