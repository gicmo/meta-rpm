SUMMARY = "generated recipe based on python-snowballstemmer srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-snowballstemmer = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-snowballstemmer-1.2.1-6.el8.noarch.rpm \
          "

SRC_URI[python3-snowballstemmer.sha256sum] = "d5dbe0d4104974537ced345acdaac2a01eb03f469af560278b495f7c08139e4d"
