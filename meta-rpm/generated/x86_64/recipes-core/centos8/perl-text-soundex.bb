SUMMARY = "generated recipe based on perl-Text-Soundex srpm"
DESCRIPTION = "Description"
LICENSE = "(CLOSED) & (GPL-2.0 | Artistic-1.0)"
RPM_LICENSE = "(Copyright only) and (GPL+ or Artistic)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Text-Soundex = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Text-Soundex = "glibc perl-Carp perl-Exporter perl-Text-Unidecode perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Text-Soundex-3.05-8.el8.x86_64.rpm \
          "

SRC_URI[perl-Text-Soundex.sha256sum] = "f386a60ceeec0a1308f2ccaa14406bb11a91a699680fc3581108f2fc581da0bf"
