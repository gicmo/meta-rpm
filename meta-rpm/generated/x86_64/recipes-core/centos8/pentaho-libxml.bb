SUMMARY = "generated recipe based on pentaho-libxml srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_pentaho-libxml = "java-1.8.0-openjdk javapackages-tools libbase libloader"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pentaho-libxml-1.1.3-17.el8.noarch.rpm \
          "

SRC_URI[pentaho-libxml.sha256sum] = "3e51605f9e9d5c8f763b2e1a1621b9e8735938d3cc0aa9672445048abb9c8547"
