SUMMARY = "generated recipe based on libtool srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libtool-ltdl m4 pkgconfig-native"
RDEPENDS_libtool = "autoconf automake bash findutils gcc info sed tar"
RPM_SONAME_REQ_libtool-ltdl-devel = "libltdl.so.7"
RPROVIDES_libtool-ltdl-devel = "libtool-ltdl-dev (= 2.4.6)"
RDEPENDS_libtool-ltdl-devel = "automake bash libtool-ltdl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libtool-2.4.6-25.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libtool-ltdl-devel-2.4.6-25.el8.x86_64.rpm \
          "
SRC_URI = "file://libtool-2.4.6-25.el8.patch file://libtool-2.4.6-25.el8-sysroot.patch"

SRC_URI[libtool.sha256sum] = "d86136888e536c735f1b8f47b1ee075bac5d5c3fbe70699c171a4bca30a48c59"
SRC_URI[libtool-ltdl-devel.sha256sum] = "d8735c2f5caf65efe1665a7ce28d237df3641f47740c6e0b572e670a21173a54"
