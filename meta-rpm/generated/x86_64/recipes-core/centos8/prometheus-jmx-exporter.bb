SUMMARY = "generated recipe based on prometheus-jmx-exporter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_prometheus-jmx-exporter = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/prometheus-jmx-exporter-0.12.0-5.el8.noarch.rpm \
          "

SRC_URI[prometheus-jmx-exporter.sha256sum] = "3760710c1d6437f483cf0cc457bd4285e5d7a0aa367fdfa13934955a4e40c73e"
