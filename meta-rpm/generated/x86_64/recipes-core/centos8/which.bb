SUMMARY = "generated recipe based on which srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_which = "libc.so.6"
RDEPENDS_which = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/which-2.21-12.el8.x86_64.rpm \
          "

SRC_URI[which.sha256sum] = "ee0cbf18628358fcd8003364fd68edbd267342196139c7ee584eb56f2e01f5fc"
