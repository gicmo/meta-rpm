SUMMARY = "generated recipe based on texi2html srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & CLOSED & (CC-BY-SA-1.0 | GPL-2.0)"
RPM_LICENSE = "GPLv2+ and OFSFDL and (CC-BY-SA or GPLv2)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_texi2html = "bash info latex2html perl-Data-Dumper perl-Exporter perl-Getopt-Long perl-PathTools perl-Text-Unidecode perl-Unicode-EastAsianWidth perl-interpreter perl-libintl-perl perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/texi2html-5.0-8.el8.noarch.rpm \
          "

SRC_URI[texi2html.sha256sum] = "9a687037dc92254a7d5c5468039ca1883c83d63fed71756318ded1cd90a50b16"
