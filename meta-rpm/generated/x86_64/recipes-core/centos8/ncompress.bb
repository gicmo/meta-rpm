SUMMARY = "generated recipe based on ncompress srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ncompress = "libc.so.6"
RDEPENDS_ncompress = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ncompress-4.2.4.4-12.el8.x86_64.rpm \
          "

SRC_URI[ncompress.sha256sum] = "dfabefb25cd5e9b81b084eb3be892da279ec423979c2c25b5d3404fbabf8b9ba"
