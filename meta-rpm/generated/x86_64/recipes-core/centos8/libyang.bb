SUMMARY = "generated recipe based on libyang srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libpcre pkgconfig-native"
RPM_SONAME_PROV_libyang = "libyang.so.0.16"
RPM_SONAME_REQ_libyang = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libm.so.6 libpcre.so.1 libpthread.so.0"
RDEPENDS_libyang = "glibc pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libyang-0.16.105-3.el8_1.2.x86_64.rpm \
          "

SRC_URI[libyang.sha256sum] = "9aa02d180a2c3c4003747161aac2f38e51f9ee1f397182a2a97fed7c574a25af"
