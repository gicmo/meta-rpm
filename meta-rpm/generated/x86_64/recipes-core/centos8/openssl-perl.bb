SUMMARY = "generated recipe based on openssl srpm"
DESCRIPTION = "Description"
LICENSE = "OpenSSL"
RPM_LICENSE = "OpenSSL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_openssl-perl = "openssl perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssl-perl-1.1.1c-15.el8.x86_64.rpm \
          "

SRC_URI[openssl-perl.sha256sum] = "b19b4d8d30b50a3f58f9f3de7cf846b3977137d9f7050873dc83913393e055dc"
