SUMMARY = "generated recipe based on apache-commons-net srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-net = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-net-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-net-3.6-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-net-javadoc-3.6-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-net.sha256sum] = "d082fb01dc9ba0c86ac7e153f46c85ec066e8f97cdcfff1113a664dae6baf0fb"
SRC_URI[apache-commons-net-javadoc.sha256sum] = "a226beb985feeb2ac89738e048d9ba42c866b57f605916914e2f9c20f09f41e6"
