SUMMARY = "generated recipe based on hunspell-yi srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-2.0 | MPL-1.1"
RPM_LICENSE = "LGPLv2+ or GPLv2+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-yi = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-yi-1.1-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-yi.sha256sum] = "05eaf50e1e28bb6b4c518588421b473cbe9bc39851fc11f324e74eab140490dc"
