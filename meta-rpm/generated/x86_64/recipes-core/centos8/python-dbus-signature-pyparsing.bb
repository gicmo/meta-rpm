SUMMARY = "generated recipe based on python-dbus-signature-pyparsing srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-dbus-signature-pyparsing = "platform-python python3-pyparsing"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-dbus-signature-pyparsing-0.03-2.el8.noarch.rpm \
          "

SRC_URI[python3-dbus-signature-pyparsing.sha256sum] = "62b5630e53652bb23f9beb8ee9a5da76393e320b431b85832db24f09a2cdf681"
