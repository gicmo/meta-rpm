SUMMARY = "generated recipe based on perl-perlfaq srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-perlfaq = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-perlfaq-5.20180605-1.el8.noarch.rpm \
          "

SRC_URI[perl-perlfaq.sha256sum] = "749d6ddca1d52398537d0a9c8f99bb6551a1f4baf725b0aa7537fbca9169959f"
