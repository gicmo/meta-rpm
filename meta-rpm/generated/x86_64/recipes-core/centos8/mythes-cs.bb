SUMMARY = "generated recipe based on mythes-cs srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-cs = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-cs-0.20070926-19.el8.noarch.rpm \
          "

SRC_URI[mythes-cs.sha256sum] = "ff50c73380022e54a60878bc3813fe0579761e32e95e9c8fdedecc232db6e5e0"
