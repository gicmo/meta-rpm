SUMMARY = "generated recipe based on spice-qxl-wddm-dod srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-qxl-wddm-dod-0.19-2.el8.noarch.rpm \
          "

SRC_URI[spice-qxl-wddm-dod.sha256sum] = "0e99509aa54964dab7dc7bc5e3a7cd26b62a49645c448628bb64499717278078"
