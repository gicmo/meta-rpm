SUMMARY = "generated recipe based on libbpf srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | BSD"
RPM_LICENSE = "LGPLv2 or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_PROV_libbpf = "libbpf.so.0"
RPM_SONAME_REQ_libbpf = "libc.so.6 libelf.so.1"
RDEPENDS_libbpf = "elfutils-libelf glibc"
RPM_SONAME_REQ_libbpf-devel = "libbpf.so.0"
RPROVIDES_libbpf-devel = "libbpf-dev (= 0.0.4)"
RDEPENDS_libbpf-devel = "elfutils-libelf-devel kernel-headers libbpf pkgconf-pkg-config"
RDEPENDS_libbpf-static = "libbpf-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libbpf-0.0.4-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libbpf-devel-0.0.4-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libbpf-static-0.0.4-5.el8.x86_64.rpm \
          "

SRC_URI[libbpf.sha256sum] = "dcfae49cecb576e3d8961bd31b3972f008a2631ecb415ece2f483f9347f5f304"
SRC_URI[libbpf-devel.sha256sum] = "f711954e82385a3290e0610b8f165612e49aa87d7b23598d86c46063e8b35b15"
SRC_URI[libbpf-static.sha256sum] = "b703cde4238c3383587d649a6a8c448a3705194234959db982b947181f227cdb"
