SUMMARY = "generated recipe based on dosfstools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_dosfstools = "libc.so.6"
RDEPENDS_dosfstools = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dosfstools-4.1-6.el8.x86_64.rpm \
          "

SRC_URI[dosfstools.sha256sum] = "40676b73567e195228ba2a8bb53692f88f88d43612564613fb168383eee57f6a"
