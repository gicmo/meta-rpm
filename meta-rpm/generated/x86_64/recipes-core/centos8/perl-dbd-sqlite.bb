SUMMARY = "generated recipe based on perl-DBD-SQLite srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native sqlite3"
RPM_SONAME_REQ_perl-DBD-SQLite = "libc.so.6 libperl.so.5.26 libpthread.so.0 libsqlite3.so.0"
RDEPENDS_perl-DBD-SQLite = "glibc perl-DBI perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-DBD-SQLite-1.58-2.el8.x86_64.rpm \
          "

SRC_URI[perl-DBD-SQLite.sha256sum] = "e53a37358feaff5ff0806934e548a6047223bd9b9d58f9a37c122f39ebd9e3db"
