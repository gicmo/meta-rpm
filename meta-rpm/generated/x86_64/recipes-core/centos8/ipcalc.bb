SUMMARY = "generated recipe based on ipcalc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ipcalc = "libc.so.6 libdl.so.2"
RDEPENDS_ipcalc = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ipcalc-0.2.4-4.el8.x86_64.rpm \
          "

SRC_URI[ipcalc.sha256sum] = "dea18976861575d40ffca814dee08a225376c7828a5afc9e5d0a383edd3d8907"
