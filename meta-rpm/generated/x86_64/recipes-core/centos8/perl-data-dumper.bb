SUMMARY = "generated recipe based on perl-Data-Dumper srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Data-Dumper = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Data-Dumper = "glibc perl-Carp perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Data-Dumper-2.167-399.el8.x86_64.rpm \
          "

SRC_URI[perl-Data-Dumper.sha256sum] = "2cd7e38beedf2751fa1a82c4441c460e7cde47dbef58b0ff2f473c7f392ac267"
