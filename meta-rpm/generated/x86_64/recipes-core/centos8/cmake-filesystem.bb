SUMMARY = "generated recipe based on cmake srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & MIT & Zlib"
RPM_LICENSE = "BSD and MIT and zlib"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cmake-filesystem-3.11.4-7.el8.x86_64.rpm \
          "

SRC_URI[cmake-filesystem.sha256sum] = "3aa18ed59034fc2d84f4370526e7ca96038be3182643e66f52eabd2834217dc7"
