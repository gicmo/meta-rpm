SUMMARY = "generated recipe based on firewalld srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/firewalld-filesystem-0.8.0-4.el8.noarch.rpm \
          "

SRC_URI[firewalld-filesystem.sha256sum] = "9e749e7e75dd8fdbbe45db6debc19553dad962b447ab86fd10ddd96517ad65d3"
