SUMMARY = "generated recipe based on grep srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libpcre pkgconfig-native"
RPM_SONAME_REQ_grep = "libc.so.6 libpcre.so.1"
RDEPENDS_grep = "bash glibc info pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grep-3.1-6.el8.x86_64.rpm \
          "

SRC_URI[grep.sha256sum] = "3f8ffe48bb481a5db7cbe42bf73b839d872351811e5df41b2f6697c61a030487"
