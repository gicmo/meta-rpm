SUMMARY = "generated recipe based on osinfo-db-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 json-glib libarchive libxml2 pkgconfig-native"
RPM_SONAME_REQ_osinfo-db-tools = "libarchive.so.13 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libpthread.so.0 libxml2.so.2"
RDEPENDS_osinfo-db-tools = "glib2 glibc gvfs json-glib libarchive libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/osinfo-db-tools-1.5.0-4.el8.x86_64.rpm \
          "

SRC_URI[osinfo-db-tools.sha256sum] = "3aba116872973d5c938be4355f705f118a43d9723bb3db9bf3b4094850e7348a"
