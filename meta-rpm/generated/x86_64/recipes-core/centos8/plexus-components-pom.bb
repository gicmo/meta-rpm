SUMMARY = "generated recipe based on plexus-components-pom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-components-pom = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-component-metadata plexus-pom"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-components-pom-1.3.1-10.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-components-pom.sha256sum] = "df0a509db9b25c7be0be9590c619f342e03c3b219533ac3b20c7da422d6fa8fb"
