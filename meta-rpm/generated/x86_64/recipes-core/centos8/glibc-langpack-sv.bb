SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-sv = "locale-base-sv-fi (= 2.28) locale-base-sv-fi.utf8 (= 2.28) locale-base-sv-fi@euro (= 2.28) locale-base-sv-se (= 2.28) locale-base-sv-se.iso885915 (= 2.28) locale-base-sv-se.utf8 (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv-fi (= 2.28) virtual-locale-sv-fi.utf8 (= 2.28) virtual-locale-sv-fi@euro (= 2.28) virtual-locale-sv-se (= 2.28) virtual-locale-sv-se.iso885915 (= 2.28) virtual-locale-sv-se.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-sv = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sv-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-sv.sha256sum] = "1d237ec5ee05cabf06467cc88af2ee90bac504177604a42a4862984a58fcbf43"
