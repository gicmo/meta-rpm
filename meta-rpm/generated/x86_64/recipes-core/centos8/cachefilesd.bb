SUMMARY = "generated recipe based on cachefilesd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cachefilesd = "libc.so.6"
RDEPENDS_cachefilesd = "bash glibc selinux-policy-minimum systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cachefilesd-0.10.10-4.el8.x86_64.rpm \
          "

SRC_URI[cachefilesd.sha256sum] = "07139488664989e19e6601a96085d0b8cdd92bb21e2b8a7d76c497232ba55e89"
