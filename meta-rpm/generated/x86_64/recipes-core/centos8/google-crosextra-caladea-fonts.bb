SUMMARY = "generated recipe based on google-crosextra-caladea-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_google-crosextra-caladea-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-crosextra-caladea-fonts-1.002-0.10.20130214.el8.noarch.rpm \
          "

SRC_URI[google-crosextra-caladea-fonts.sha256sum] = "bbe1927db54119e6886b4ae92dcc277d5aed955fd6a7527b88847bb494654141"
