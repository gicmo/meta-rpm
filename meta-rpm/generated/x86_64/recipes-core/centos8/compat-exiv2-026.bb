SUMMARY = "generated recipe based on compat-exiv2-026 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "expat libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_compat-exiv2-026 = "libexiv2.so.26"
RPM_SONAME_REQ_compat-exiv2-026 = "libc.so.6 libdl.so.2 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_compat-exiv2-026 = "expat glibc libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/compat-exiv2-026-0.26-3.el8.x86_64.rpm \
          "

SRC_URI[compat-exiv2-026.sha256sum] = "4471353170caaf9f6849312e8b1385632b7ff032fba18abf7e69ea8ad55da4d7"
