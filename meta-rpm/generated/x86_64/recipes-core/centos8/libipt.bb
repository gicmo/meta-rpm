SUMMARY = "generated recipe based on libipt srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libipt = "libipt.so.1"
RPM_SONAME_REQ_libipt = "libc.so.6 libpthread.so.0"
RDEPENDS_libipt = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libipt-1.6.1-8.el8.x86_64.rpm \
          "

SRC_URI[libipt.sha256sum] = "8560168d424b63816bfaae1c8b3d2658c60de0ccf3b572860bc6738d25a76a1b"
