SUMMARY = "generated recipe based on gdisk srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libuuid ncurses pkgconfig-native popt"
RPM_SONAME_REQ_gdisk = "libc.so.6 libgcc_s.so.1 libm.so.6 libncursesw.so.6 libpopt.so.0 libstdc++.so.6 libtinfo.so.6 libuuid.so.1"
RDEPENDS_gdisk = "glibc libgcc libstdc++ libuuid ncurses-libs popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gdisk-1.0.3-6.el8.x86_64.rpm \
          "

SRC_URI[gdisk.sha256sum] = "fa0b90c4da7f7ca8bf40055be5641a2c57708931fec5f760a2f8944325669fe9"
