SUMMARY = "generated recipe based on logrotate srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl libselinux pkgconfig-native popt"
RPM_SONAME_REQ_logrotate = "libacl.so.1 libc.so.6 libpopt.so.0 libselinux.so.1"
RDEPENDS_logrotate = "bash coreutils glibc libacl libselinux popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/logrotate-3.14.0-3.el8.x86_64.rpm \
          "

SRC_URI[logrotate.sha256sum] = "caf32573dbceb361203d804aa8b26ba4bc859204c8799a36ae1a5b34a69f2afc"
