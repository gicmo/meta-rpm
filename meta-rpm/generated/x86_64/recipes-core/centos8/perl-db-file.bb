SUMMARY = "generated recipe based on perl-DB_File srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "db perl pkgconfig-native"
RPM_SONAME_REQ_perl-DB_File = "libc.so.6 libdb-5.3.so libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-DB_File = "glibc libdb perl-Carp perl-Exporter perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-DB_File-1.842-1.el8.x86_64.rpm \
          "

SRC_URI[perl-DB_File.sha256sum] = "9b51aef8ac6f5699a1f961fe176223ed6df3d632809bdd26b5ffae481f9e3284"
