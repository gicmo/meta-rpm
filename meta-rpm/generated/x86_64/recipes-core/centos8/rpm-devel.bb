SUMMARY = "generated recipe based on rpm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+ with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
DEPENDS = "acl audit-libs bzip2 db elfutils file libcap lua openssl pkgconfig-native popt xz zlib zstd"
RPM_SONAME_REQ_rpm-build = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdw.so.1 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libmagic.so.1 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmbuild.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-build = "audit-libs bash binutils bzip2 bzip2-libs cpio diffutils elfutils elfutils-libelf elfutils-libs file file-libs findutils gawk gdb-headless glibc grep gzip libacl libcap libdb libzstd lua-libs openssl-libs patch pkgconf-pkg-config popt redhat-rpm-config rpm rpm-build-libs rpm-libs sed tar unzip xz xz-libs zlib zstd"
RPM_SONAME_REQ_rpm-devel = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmbuild.so.8 librpmio.so.8 librpmsign.so.8 libz.so.1 libzstd.so.1"
RPROVIDES_rpm-devel = "rpm-dev (= 4.14.2)"
RDEPENDS_rpm-devel = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd libzstd-devel lua-libs openssl-libs pkgconf-pkg-config popt popt-devel rpm rpm-build-libs rpm-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rpm-build-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-devel-4.14.2-37.el8.x86_64.rpm \
          "

SRC_URI[rpm-build.sha256sum] = "fbbdb43b9f0d36fab62ccf53c7e5d5cd6dcd5c840c8d6ca1e338b1366c227bf1"
SRC_URI[rpm-devel.sha256sum] = "4b78e8cf3c3a7f12b5f4ab1a78c7cc4ed760b3f5df9fa1c0a0f93b1881d76f75"
