SUMMARY = "generated recipe based on fusesource-pom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_fusesource-pom = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/fusesource-pom-1.11-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[fusesource-pom.sha256sum] = "fa4c651607542e4956a46dc6646a56bce905b3f0d8d5c0b914c1aab8acacc5ec"
