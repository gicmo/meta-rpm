SUMMARY = "generated recipe based on nss_nis srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnsl2 libtirpc pkgconfig-native"
RPM_SONAME_PROV_nss_nis = "libnss_nis.so.2"
RPM_SONAME_REQ_nss_nis = "libc.so.6 libnsl.so.2 libtirpc.so.3"
RDEPENDS_nss_nis = "glibc libnsl2 libtirpc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nss_nis-3.0-8.el8.x86_64.rpm \
          "

SRC_URI[nss_nis.sha256sum] = "0568970eb2d12a42f540a67792a058c7af9ddc4b1107db9c41a707eb12a2496d"
