SUMMARY = "generated recipe based on perl-Data-Dump srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Data-Dump = "perl-Carp perl-Exporter perl-Term-ANSIColor perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Data-Dump-1.23-7.el8.noarch.rpm \
          "

SRC_URI[perl-Data-Dump.sha256sum] = "5d5a3744a60ffe78d3e94cd6c47917c3614f65cd3d5c81261338be22fbeecff1"
