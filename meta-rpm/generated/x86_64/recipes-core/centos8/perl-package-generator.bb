SUMMARY = "generated recipe based on perl-Package-Generator srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Package-Generator = "perl-Carp perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Package-Generator-1.106-11.el8.noarch.rpm \
          "

SRC_URI[perl-Package-Generator.sha256sum] = "c7a2c6b9e518327cdc1be3658cf164b6f7a3996fcdf303ffe556a8b212baac3f"
