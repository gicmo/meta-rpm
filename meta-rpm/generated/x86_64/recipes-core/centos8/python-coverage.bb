SUMMARY = "generated recipe based on python-coverage srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & MIT & (MIT | GPL-2.0)"
RPM_LICENSE = "ASL 2.0 and MIT and (MIT or GPL)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_platform-python-coverage = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_platform-python-coverage = "glibc platform-python platform-python-setuptools python3-libs"
RDEPENDS_python3-coverage = "bash platform-python platform-python-coverage python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/platform-python-coverage-4.5.1-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-coverage-4.5.1-7.el8.x86_64.rpm \
          "

SRC_URI[platform-python-coverage.sha256sum] = "b2640ea6d5a3951d1489705145271d6d8d38a0a8f43b34e1947f8de9e763544c"
SRC_URI[python3-coverage.sha256sum] = "3d951d0ddfd51fd4a0b1837511d48fe7c3795d3b8a6fec3d8426e8130e2a51e4"
