SUMMARY = "generated recipe based on hunspell-lb srpm"
DESCRIPTION = "Description"
LICENSE = "EUPL-1.1"
RPM_LICENSE = "EUPL 1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-lb = "hunspell"
RDEPENDS_mythes-lb = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-lb-0.20121128-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-lb-0.20121128-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-lb.sha256sum] = "4288480ae063cb6fcfaf473c5343da7620d0c1fcc62b8073de504312f8e1f194"
SRC_URI[mythes-lb.sha256sum] = "91c21b1c87331d3db06944a1f86756bbaf9231f576cd4cc0cdb119f597497972"
