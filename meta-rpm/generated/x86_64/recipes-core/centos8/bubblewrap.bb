SUMMARY = "generated recipe based on bubblewrap srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcap libgcc libselinux pkgconfig-native"
RPM_SONAME_REQ_bubblewrap = "libc.so.6 libcap.so.2 libgcc_s.so.1 libselinux.so.1"
RDEPENDS_bubblewrap = "glibc libcap libgcc libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bubblewrap-0.4.0-1.el8.x86_64.rpm \
          "

SRC_URI[bubblewrap.sha256sum] = "9e78ec1230b9ec69ef8e3ad0484cbe3b5c36cfc214d90f890a49093b22df2948"
