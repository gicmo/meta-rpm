SUMMARY = "generated recipe based on perl-Devel-LexAlias srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-LexAlias = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-LexAlias = "glibc perl-Devel-Caller perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Devel-LexAlias-0.05-16.el8.x86_64.rpm \
          "

SRC_URI[perl-Devel-LexAlias.sha256sum] = "7f0bed65f225c91c8035c1d9c817a069587acba67d1d4ffbf118d35e61160cca"
