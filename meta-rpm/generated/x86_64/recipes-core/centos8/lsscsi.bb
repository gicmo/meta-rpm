SUMMARY = "generated recipe based on lsscsi srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lsscsi = "libc.so.6"
RDEPENDS_lsscsi = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lsscsi-0.30-1.el8.x86_64.rpm \
          "

SRC_URI[lsscsi.sha256sum] = "469d2db0a8e2af4b372cf4f9112977af027eb2dd5cb5e8fa9d6ba7b334f56aeb"
