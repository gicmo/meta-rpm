SUMMARY = "generated recipe based on hspell srpm"
DESCRIPTION = "Description"
LICENSE = "AGPL-3.0"
RPM_LICENSE = "AGPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_hspell = "libhspell.so.0"
RPM_SONAME_REQ_hspell = "libc.so.6 libz.so.1"
RDEPENDS_hspell = "glibc perl-Getopt-Long perl-IO perl-interpreter perl-libs zlib"
RDEPENDS_hunspell-he = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hspell-1.4-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-he-1.4-6.el8.x86_64.rpm \
          "

SRC_URI[hspell.sha256sum] = "05202eb1c017bf60bc3b5a05a997dc89db310bed78c11c8c22c7f8d50f3619d2"
SRC_URI[hunspell-he.sha256sum] = "b4d56217f0288726be9069f97c34807201c2dbffba2c655a26275802599a7cc8"
