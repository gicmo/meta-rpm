SUMMARY = "generated recipe based on perl-Devel-CallChecker srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-CallChecker = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-CallChecker = "glibc perl-DynaLoader-Functions perl-Exporter perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Devel-CallChecker-0.008-3.el8.x86_64.rpm \
          "

SRC_URI[perl-Devel-CallChecker.sha256sum] = "b173e3b4af17574349299f2b049ef9dc21a883783ff37618ec7b40c7a99db909"
