SUMMARY = "generated recipe based on gnome-common srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_gnome-common = "autoconf autoconf-archive automake bash gettext libtool pkgconf-pkg-config yelp-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gnome-common-3.18.0-5.el8.noarch.rpm \
          "

SRC_URI[gnome-common.sha256sum] = "7c2ef656995a9410ef1b03c2e5b5a2e31cc7824057fff5501181a5aea72a99cf"
