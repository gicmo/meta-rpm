SUMMARY = "generated recipe based on perl-Devel-CheckLib srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Devel-CheckLib = "perl-Exporter perl-File-Temp perl-PathTools perl-Text-ParseWords perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Devel-CheckLib-1.11-5.el8.noarch.rpm \
          "

SRC_URI[perl-Devel-CheckLib.sha256sum] = "a1d99c3ef4ea454ad9d736bf5c89c61c8bcecc9f0ab25947d7c93288cec45df2"
