SUMMARY = "generated recipe based on byaccj srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_byaccj = "libc.so.6"
RDEPENDS_byaccj = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/byaccj-1.15-17.module_el8.0.0+30+832da3a1.x86_64.rpm \
          "

SRC_URI[byaccj.sha256sum] = "48c11fe485643110e15d38412b9c33e7e31e2c0436fabb5a972630ea72c4b082"
