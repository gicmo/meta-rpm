SUMMARY = "generated recipe based on dbxtool srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "efivar pkgconfig-native popt"
RPM_SONAME_REQ_dbxtool = "libc.so.6 libefivar.so.1 libpopt.so.0 libpthread.so.0"
RDEPENDS_dbxtool = "bash efivar efivar-libs glibc popt systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dbxtool-8-5.el8.x86_64.rpm \
          "

SRC_URI[dbxtool.sha256sum] = "69fc3075751693fe23c0658d1e97c0e14366d8833a78ab75b951596b07345827"
