SUMMARY = "generated recipe based on ledmon srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native sg3-utils systemd-libs"
RPM_SONAME_REQ_ledmon = "libc.so.6 librt.so.1 libsgutils2.so.2 libudev.so.1"
RDEPENDS_ledmon = "glibc sg3_utils-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ledmon-0.93-1.el8.x86_64.rpm \
          "

SRC_URI[ledmon.sha256sum] = "5d02dbafb542d4dddf9f8475db14aed9f395ecafcd2b8baa1b6e260942a5aa04"
