SUMMARY = "generated recipe based on hunspell-shs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-shs = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-shs-0.20090828-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-shs.sha256sum] = "c421d276396731defdfd3d1f2d7f1b6a0987ecd295f936cd32f43cf433203daf"
