SUMMARY = "generated recipe based on flute srpm"
DESCRIPTION = "Description"
LICENSE = "W3C & LGPL-2.0"
RPM_LICENSE = "W3C and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_flute = "java-1.8.0-openjdk-headless javapackages-tools sac"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flute-1.3.0-18.OOo31.el8.noarch.rpm \
          "

SRC_URI[flute.sha256sum] = "36dafd17eb2580d8b114d34731ee8e64fee1fd1e8e6d920065ed27a0a70c7242"
