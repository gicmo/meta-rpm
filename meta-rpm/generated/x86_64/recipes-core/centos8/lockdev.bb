SUMMARY = "generated recipe based on lockdev srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lockdev = "liblockdev.so.1"
RPM_SONAME_REQ_lockdev = "libc.so.6"
RDEPENDS_lockdev = "bash glibc shadow-utils systemd"
RPM_SONAME_REQ_lockdev-devel = "liblockdev.so.1"
RPROVIDES_lockdev-devel = "lockdev-dev (= 1.0.4)"
RDEPENDS_lockdev-devel = "lockdev pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lockdev-1.0.4-0.28.20111007git.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lockdev-devel-1.0.4-0.28.20111007git.el8.x86_64.rpm \
          "

SRC_URI[lockdev.sha256sum] = "fcc180b2bd66d79100b964ac64f64b78479e81358604767396ea966f964b17f7"
SRC_URI[lockdev-devel.sha256sum] = "c82b0f28eecb3f27b8a1439b1e57105901e7f18c9f98799dddbcc6e77bd3e687"
