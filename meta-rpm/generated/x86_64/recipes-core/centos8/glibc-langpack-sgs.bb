SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-sgs = "locale-base-sgs-lt (= 2.28) virtual-locale-sgs (= 2.28) virtual-locale-sgs-lt (= 2.28)"
RDEPENDS_glibc-langpack-sgs = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sgs-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-sgs.sha256sum] = "a99c9fe69d31bc38d5802bab63f61a423a1badc85dc658d8561eb91290642a35"
