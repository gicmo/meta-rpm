SUMMARY = "generated recipe based on lohit-bengali-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-bengali-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-bengali-fonts-2.91.5-3.el8.noarch.rpm \
          "

SRC_URI[lohit-bengali-fonts.sha256sum] = "d0097994334b5f77b4caedd63014c75f87c272b9738dd7a50ff940d43ebd587a"
