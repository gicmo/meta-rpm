SUMMARY = "generated recipe based on nafees-web-naskh-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "BitstreamVera"
RPM_LICENSE = "Bitstream Vera"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_nafees-web-naskh-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nafees-web-naskh-fonts-1.2-18.el8.noarch.rpm \
          "

SRC_URI[nafees-web-naskh-fonts.sha256sum] = "694a414a4dabca7fa7164d99fdf4b74ca8fffc67448a3ff690a8b0d57ccacc72"
