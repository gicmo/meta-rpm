SUMMARY = "generated recipe based on diffutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_diffutils = "libc.so.6"
RDEPENDS_diffutils = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/diffutils-3.6-6.el8.x86_64.rpm \
          "

SRC_URI[diffutils.sha256sum] = "c515d78c64a93d8b469593bff5800eccd50f24b16697ab13bdce81238c38eb77"
