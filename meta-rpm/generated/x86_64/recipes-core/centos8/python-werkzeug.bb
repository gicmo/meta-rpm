SUMMARY = "generated recipe based on python-werkzeug srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-werkzeug = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-werkzeug-0.12.2-4.el8.noarch.rpm \
          "

SRC_URI[python3-werkzeug.sha256sum] = "0b55755bc00fd85fc304815fba16a1b62d02501db2e98f44b7663f1652a1f814"
