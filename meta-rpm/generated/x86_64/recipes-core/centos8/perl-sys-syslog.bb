SUMMARY = "generated recipe based on perl-Sys-Syslog srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-Syslog = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-Syslog = "glibc perl-Carp perl-Exporter perl-Socket perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Sys-Syslog-0.35-397.el8.x86_64.rpm \
          "

SRC_URI[perl-Sys-Syslog.sha256sum] = "cd8346b8dd5970bc074d1a59f54d892d3fb1a5b665e513325f809e6fb29bf33d"
