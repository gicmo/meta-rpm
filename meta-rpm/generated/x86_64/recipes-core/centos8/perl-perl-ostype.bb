SUMMARY = "generated recipe based on perl-Perl-OSType srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Perl-OSType = "perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Perl-OSType-1.010-396.el8.noarch.rpm \
          "

SRC_URI[perl-Perl-OSType.sha256sum] = "750244c66bc3d17c096af0605da88e041c9e66827029a34d789cb37bad68d33d"
