SUMMARY = "generated recipe based on python-lxml srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxml2 libxslt pkgconfig-native platform-python3 zlib"
RPM_SONAME_REQ_python3-lxml = "libc.so.6 libexslt.so.0 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libxml2.so.2 libxslt.so.1 libz.so.1"
RDEPENDS_python3-lxml = "glibc libxml2 libxslt platform-python python3-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-lxml-4.2.3-1.el8.x86_64.rpm \
          "

SRC_URI[python3-lxml.sha256sum] = "18989b6c88a21f760acd269154bb305b3d6ebc69c3d2fd8e24ba7b51087d79e4"
