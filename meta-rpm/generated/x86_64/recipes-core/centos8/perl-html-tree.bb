SUMMARY = "generated recipe based on perl-HTML-Tree srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTML-Tree = "perl-Carp perl-Exporter perl-HTML-Parser perl-HTML-Tagset perl-Pod-Usage perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-HTML-Tree-5.07-2.el8.noarch.rpm \
          "

SRC_URI[perl-HTML-Tree.sha256sum] = "899f1e0ee13dc8fb6bf0b23cb077509eddaaefa47f25a606659116ff013abc3c"
