SUMMARY = "generated recipe based on setroubleshoot-plugins srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_setroubleshoot-plugins = "setroubleshoot-server"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/setroubleshoot-plugins-3.3.11-2.el8.noarch.rpm \
          "

SRC_URI[setroubleshoot-plugins.sha256sum] = "8a3c31c6886261f7d67eb9128c84aaaec86c57c0584128059cda892e7984e230"
