SUMMARY = "generated recipe based on mod_http2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnghttp2 openssl pkgconfig-native"
RPM_SONAME_REQ_mod_http2 = "libc.so.6 libcrypto.so.1.1 libnghttp2.so.14"
RDEPENDS_mod_http2 = "glibc httpd libnghttp2 openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_http2-1.11.3-3.module_el8.2.0+486+c01050f0.1.x86_64.rpm \
          "

SRC_URI[mod_http2.sha256sum] = "08d8f81580ff2d5aa33507c9a3c16cdfbcf9236c4017213f2db5640b5cb9176d"
