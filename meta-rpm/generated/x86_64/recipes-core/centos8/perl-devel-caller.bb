SUMMARY = "generated recipe based on perl-Devel-Caller srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-Caller = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-Caller = "glibc perl-Exporter perl-PadWalker perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Devel-Caller-2.06-15.el8.x86_64.rpm \
          "

SRC_URI[perl-Devel-Caller.sha256sum] = "9eb2e42890edad380c0188b2e5744d88ef25b17154d904daa4d194035c678214"
