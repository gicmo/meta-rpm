SUMMARY = "generated recipe based on tar srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl libselinux pkgconfig-native"
RPM_SONAME_REQ_tar = "libacl.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_tar = "bash glibc info libacl libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tar-1.30-4.el8.x86_64.rpm \
          "

SRC_URI[tar.sha256sum] = "2b861aa67c21b6ca695bbff7a3967f89c3b0af53271552ec3eab21af136b0dbb"
