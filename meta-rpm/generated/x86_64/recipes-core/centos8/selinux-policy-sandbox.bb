SUMMARY = "generated recipe based on selinux-policy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_selinux-policy-sandbox = "bash selinux-policy-minimum selinux-policy-targeted"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/selinux-policy-sandbox-3.14.3-41.el8_2.8.noarch.rpm \
          "

SRC_URI[selinux-policy-sandbox.sha256sum] = "298de058f7adecfaed18b0873dce2092d3621c475383b79311b14972e918d774"
