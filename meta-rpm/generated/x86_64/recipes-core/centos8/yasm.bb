SUMMARY = "generated recipe based on yasm srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & (GPL-2.0 | Artistic-1.0 | LGPL-2.0) & LGPL-2.0"
RPM_LICENSE = "BSD and (GPLv2+ or Artistic or LGPLv2+) and LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_yasm = "libc.so.6"
RDEPENDS_yasm = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/yasm-1.3.0-7.el8.x86_64.rpm \
          "

SRC_URI[yasm.sha256sum] = "fc3420f64828319a8b7872400396454c2ed64ff5b3086ae0b47e68be5e314d97"
