SUMMARY = "generated recipe based on perl-DateTime-TimeZone srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-TimeZone = "perl-Class-Singleton perl-DateTime perl-DateTime-TimeZone-Tzfile perl-Module-Runtime perl-Params-ValidationCompiler perl-PathTools perl-Scalar-List-Utils perl-Specio perl-Try-Tiny perl-constant perl-interpreter perl-libs perl-namespace-autoclean perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-TimeZone-2.19-1.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-TimeZone.sha256sum] = "ac91380ed9d559c82bd9f03cb443572ad6ccdbe62d48097a15968ada295e1727"
