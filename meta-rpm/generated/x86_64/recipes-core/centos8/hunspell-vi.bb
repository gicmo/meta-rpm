SUMMARY = "generated recipe based on hunspell-vi srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-vi = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-vi-0.20120418-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-vi.sha256sum] = "f89c62b627032dcf609412f056cf83eddf6a7fad85457844ded50b867673c0a1"
