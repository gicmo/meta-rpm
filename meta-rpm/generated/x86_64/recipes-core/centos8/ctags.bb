SUMMARY = "generated recipe based on ctags srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0 & CLOSED"
RPM_LICENSE = "GPLv2+ and LGPLv2+ and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ctags = "libc.so.6"
RDEPENDS_ctags = "glibc"
RDEPENDS_ctags-etags = "bash chkconfig ctags"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ctags-5.8-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ctags-etags-5.8-22.el8.x86_64.rpm \
          "

SRC_URI[ctags.sha256sum] = "33d38f792feec9c1b1d0fcd78f8b29c959dc6e0c8474a3bae8c8399840997347"
SRC_URI[ctags-etags.sha256sum] = "d563772996d6bcb0bf19ddaaec885d1754493a825329be3f715b3dffa7f1ad37"
