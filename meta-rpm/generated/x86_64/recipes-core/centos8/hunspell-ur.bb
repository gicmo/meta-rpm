SUMMARY = "generated recipe based on hunspell-ur srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ur = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ur-0.64-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-ur.sha256sum] = "cb67ed6313557979e12aa6b93a0662038c8436a72c34bbca0e8c074f09170763"
