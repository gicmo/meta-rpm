SUMMARY = "generated recipe based on sscg srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ding-libs libtalloc openssl pkgconfig-native popt"
RPM_SONAME_REQ_sscg = "libc.so.6 libcrypto.so.1.1 libpath_utils.so.1 libpopt.so.0 libssl.so.1.1 libtalloc.so.2"
RDEPENDS_sscg = "glibc libpath_utils libtalloc openssl-libs popt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sscg-2.3.3-14.el8.x86_64.rpm \
          "

SRC_URI[sscg.sha256sum] = "6fded933d86737a21c5cd77399e68e3912125c2da1afb95592e7089ba5594056"
