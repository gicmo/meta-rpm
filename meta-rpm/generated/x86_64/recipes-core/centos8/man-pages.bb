SUMMARY = "generated recipe based on man-pages srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & BSD & MIT & CLOSED & CLOSED"
RPM_LICENSE = "GPL+ and GPLv2+ and BSD and MIT and Copyright only and IEEE"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/man-pages-4.15-6.el8.x86_64.rpm \
          "

SRC_URI[man-pages.sha256sum] = "67a86aa6105425a06b01f753121d4e0e89214bae60a1f7c9e50679d2001cef40"
