SUMMARY = "generated recipe based on perl-AnyEvent srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-AnyEvent = "perl-Carp perl-Errno perl-Exporter perl-File-Temp perl-Net-SSLeay perl-Scalar-List-Utils perl-Socket perl-Time-HiRes perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-AnyEvent-7.14-6.el8.x86_64.rpm \
          "

SRC_URI[perl-AnyEvent.sha256sum] = "0d1930bbdcd090965c846aeaeac6bcb8eca240c9115b72086085608eefd712ec"
