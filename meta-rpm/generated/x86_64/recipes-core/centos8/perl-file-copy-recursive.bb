SUMMARY = "generated recipe based on perl-File-Copy-Recursive srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Copy-Recursive = "perl-Carp perl-Exporter perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-File-Copy-Recursive-0.40-3.el8.noarch.rpm \
          "

SRC_URI[perl-File-Copy-Recursive.sha256sum] = "a40bb5b1c14d0bfda4973fed58bd64fb9c07628c16e08e622b6231008e9686e1"
