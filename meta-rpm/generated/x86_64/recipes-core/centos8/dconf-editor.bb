SUMMARY = "generated recipe based on dconf-editor srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dconf glib-2.0 gtk+3 pkgconfig-native"
RPM_SONAME_REQ_dconf-editor = "libc.so.6 libdconf.so.1 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpthread.so.0"
RDEPENDS_dconf-editor = "dconf glib2 glibc gtk3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dconf-editor-3.28.0-1.el8.x86_64.rpm \
          "

SRC_URI[dconf-editor.sha256sum] = "045829c551836bd08482bc0ef419a6c9e3f3b1ffb1010b8759f46dc34d4527ea"
