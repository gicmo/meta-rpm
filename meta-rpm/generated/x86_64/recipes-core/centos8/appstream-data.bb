SUMMARY = "generated recipe based on appstream-data srpm"
DESCRIPTION = "Description"
LICENSE = "CC0-1.0 & CC-BY-1.0  & CC-BY-SA-1.0 & GFDL-1.1"
RPM_LICENSE = "CC0 and CC-BY and CC-BY-SA and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/appstream-data-8-20191129.el8.noarch.rpm \
          "

SRC_URI[appstream-data.sha256sum] = "ef39e4d234034d4416a8fc9bc9fd18e85ea859efe340e5429e2076f4402ceb1f"
