SUMMARY = "generated recipe based on mythes-ro srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-ro = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-ro-3.3-13.el8.noarch.rpm \
          "

SRC_URI[mythes-ro.sha256sum] = "9b824c63a57be3266164313c2c32b62ba7ee674b3922466fba877b1b5c7adac0"
