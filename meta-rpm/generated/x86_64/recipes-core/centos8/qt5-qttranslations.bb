SUMMARY = "generated recipe based on qt5-qttranslations srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0 & GFDL-1.1"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttranslations-5.12.5-1.el8.noarch.rpm \
          "

SRC_URI[qt5-qttranslations.sha256sum] = "cbea9b6e383ad7f2165a24313170601bfdb4612835b8476eda743ad9078330a0"
