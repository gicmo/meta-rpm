SUMMARY = "generated recipe based on perl-Class-Method-Modifiers srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Method-Modifiers = "perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Class-Method-Modifiers-2.12-8.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Method-Modifiers.sha256sum] = "2daebfaf599af6f4174d3504af5028ddb1c2a29bfaf894db1fca4f4d9bcfab41"
