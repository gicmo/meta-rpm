SUMMARY = "generated recipe based on jbig2dec srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_jbig2dec-libs = "libjbig2dec.so.0"
RPM_SONAME_REQ_jbig2dec-libs = "libc.so.6 libm.so.6"
RDEPENDS_jbig2dec-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jbig2dec-libs-0.14-4.el8_2.x86_64.rpm \
          "

SRC_URI[jbig2dec-libs.sha256sum] = "6525253f1b515ff27d61847fa45b730f09d7064d2d3747eda535c4f946be0958"
