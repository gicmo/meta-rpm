SUMMARY = "generated recipe based on python-whoosh srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-whoosh = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-whoosh-2.7.4-9.el8.noarch.rpm \
          "

SRC_URI[python3-whoosh.sha256sum] = "fd20dbf93b48e776d1fe0d81d1f5b2dc952ad1066a1484c2e2a9f4e7d7904777"
