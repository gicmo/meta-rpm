SUMMARY = "generated recipe based on iperf3 srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_iperf3 = "libiperf.so.0"
RPM_SONAME_REQ_iperf3 = "libc.so.6 libm.so.6"
RDEPENDS_iperf3 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/iperf3-3.5-3.el8.x86_64.rpm \
          "

SRC_URI[iperf3.sha256sum] = "62985d1342cb34ff480ca12d2ad08e6e2c76a34c22740aaf76aff93d8fd47442"
