SUMMARY = "generated recipe based on diffstat srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_diffstat = "libc.so.6"
RDEPENDS_diffstat = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/diffstat-1.61-7.el8.x86_64.rpm \
          "

SRC_URI[diffstat.sha256sum] = "2cb56b63aaa9bee40536b1b602b129ab70fdff872e71e7d0085424068d15650e"
