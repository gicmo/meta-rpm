SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-mai = "locale-base-mai-in (= 2.28) locale-base-mai-np (= 2.28) virtual-locale-mai (= 2.28) virtual-locale-mai (= 2.28) virtual-locale-mai-in (= 2.28) virtual-locale-mai-np (= 2.28)"
RDEPENDS_glibc-langpack-mai = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mai-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-mai.sha256sum] = "de0a7b9563b7c894b44ad6811803db6443fc54f767af5f1c09f0f0cb2668bd59"
