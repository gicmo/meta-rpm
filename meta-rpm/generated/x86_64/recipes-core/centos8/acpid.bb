SUMMARY = "generated recipe based on acpid srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_acpid = "libc.so.6"
RDEPENDS_acpid = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/acpid-2.0.30-2.el8.x86_64.rpm \
          "

SRC_URI[acpid.sha256sum] = "b5d5d5308eb73446bfd1ad716d4c34b4b90b54e945ec925c7703a6d43285a618"
