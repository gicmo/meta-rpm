SUMMARY = "generated recipe based on xorg-x11-drv-ati srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libdrm libpciaccess mesa pkgconfig-native systemd-libs"
RPM_SONAME_REQ_xorg-x11-drv-ati = "libc.so.6 libdrm_radeon.so.1 libgbm.so.1 libpciaccess.so.0 libudev.so.1"
RDEPENDS_xorg-x11-drv-ati = "glibc libdrm libpciaccess mesa-libgbm systemd-libs xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-ati-19.0.1-2.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-ati.sha256sum] = "7ce095c11063b66f12bb4b065a5b5ad1d29810a63b7a08594dea122a1cbb1bc2"
