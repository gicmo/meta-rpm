SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-et = "locale-base-et-ee (= 2.28) locale-base-et-ee.iso885915 (= 2.28) locale-base-et-ee.utf8 (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et-ee (= 2.28) virtual-locale-et-ee.iso885915 (= 2.28) virtual-locale-et-ee.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-et = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-et-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-et.sha256sum] = "7350adee0f89a307ee5c96a9e7660b9c50a18952588817a1da8e8e38671d6124"
