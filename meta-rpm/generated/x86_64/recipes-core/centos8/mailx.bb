SUMMARY = "generated recipe based on mailx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & MPL-1.1"
RPM_LICENSE = "BSD with advertising and MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "krb5-libs openssl pkgconfig-native"
RPM_SONAME_REQ_mailx = "libc.so.6 libcrypto.so.1.1 libgssapi_krb5.so.2 libssl.so.1.1"
RDEPENDS_mailx = "bash glibc krb5-libs openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mailx-12.5-29.el8.x86_64.rpm \
          "

SRC_URI[mailx.sha256sum] = "243b06fbe72abf350571af542df35234d640d58a8c37019c664cb8dc518f953e"
