SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-id = "locale-base-id-id (= 2.28) locale-base-id-id.utf8 (= 2.28) virtual-locale-id (= 2.28) virtual-locale-id (= 2.28) virtual-locale-id-id (= 2.28) virtual-locale-id-id.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-id = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-id-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-id.sha256sum] = "1ac7966c3f42bc12e3a480aa0333dc59aeda2a147de29995d2e8ecf9fe7d61b0"
