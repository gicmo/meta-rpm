SUMMARY = "generated recipe based on perl-libwww-perl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-libwww-perl = "perl-Carp perl-Data-Dump perl-Digest-MD5 perl-Encode perl-Encode-Locale perl-Exporter perl-File-Listing perl-Getopt-Long perl-HTML-Parser perl-HTTP-Cookies perl-HTTP-Date perl-HTTP-Message perl-HTTP-Negotiate perl-IO perl-LWP-MediaTypes perl-MIME-Base64 perl-NTLM perl-Net-HTTP perl-PathTools perl-Scalar-List-Utils perl-Try-Tiny perl-URI perl-WWW-RobotRules perl-interpreter perl-libnet perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-libwww-perl-6.34-1.el8.noarch.rpm \
          "

SRC_URI[perl-libwww-perl.sha256sum] = "a458d5d4c604e9ccae50b381b0bd75b756e2297ba06f1f21fa7f0244754dd25b"
