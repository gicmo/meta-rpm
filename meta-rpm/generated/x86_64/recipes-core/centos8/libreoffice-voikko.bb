SUMMARY = "generated recipe based on libreoffice-voikko srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_libreoffice-voikko = "libreoffice-core libreoffice-pyuno python3-libvoikko"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-voikko-5.0-2.el8.x86_64.rpm \
          "

SRC_URI[libreoffice-voikko.sha256sum] = "3cb47482d57e3d9caf835a8b1d53a1dedb1ea9d16a881a2808bc89beb27b66c5"
