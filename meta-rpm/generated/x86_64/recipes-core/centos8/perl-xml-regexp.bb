SUMMARY = "generated recipe based on perl-XML-RegExp srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-RegExp = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-XML-RegExp-0.04-14.el8.noarch.rpm \
          "

SRC_URI[perl-XML-RegExp.sha256sum] = "059d13b6d3e2742805f20e3d28db64d2314ae0f7423c42ad014048c18760d077"
