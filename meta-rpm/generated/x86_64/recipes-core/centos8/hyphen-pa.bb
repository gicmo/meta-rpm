SUMMARY = "generated recipe based on hyphen-pa srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-pa = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-pa-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-pa.sha256sum] = "eabfe20dcc0c0e9e52c128e6c75d01ea182cf69ac13bf3bd23079cf5a86625ca"
