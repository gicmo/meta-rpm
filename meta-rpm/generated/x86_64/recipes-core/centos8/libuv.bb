SUMMARY = "generated recipe based on libuv srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & BSD & ISC"
RPM_LICENSE = "MIT and BSD and ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libuv = "libuv.so.1"
RPM_SONAME_REQ_libuv = "libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_libuv = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libuv-1.23.1-1.el8.x86_64.rpm \
          "

SRC_URI[libuv.sha256sum] = "c766757b8aff356fb0256cfc2739295fcdcda0debec74edbd1095fa46d87d72a"
