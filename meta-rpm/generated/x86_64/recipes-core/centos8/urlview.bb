SUMMARY = "generated recipe based on urlview srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_urlview = "libc.so.6 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_urlview = "bash glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/urlview-0.9-23.20131022git08767a.el8.x86_64.rpm \
          "

SRC_URI[urlview.sha256sum] = "580bed394e44b89d92ae923310fe2fb549c0ff537a4b709e26290fc06104dd06"
