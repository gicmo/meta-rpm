SUMMARY = "generated recipe based on geronimo-parent-poms srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_geronimo-parent-poms = "java-1.8.0-openjdk-headless javapackages-filesystem maven-compiler-plugin maven-jar-plugin maven-plugin-bundle"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/geronimo-parent-poms-1.6-25.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[geronimo-parent-poms.sha256sum] = "b908e2de3dd23b337cbe4a8dd3d42addeda47fdefdcbd3a4b21bbff4d65bf784"
