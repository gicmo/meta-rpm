SUMMARY = "generated recipe based on hunspell-eu srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-eu = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-eu-0.20080507-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-eu.sha256sum] = "4185d9eedce72f7724965021e46c2b721e9e4cf942fae85aa6b5d06171bb033f"
