SUMMARY = "generated recipe based on perl-Test-Taint srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Test-Taint = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Test-Taint = "glibc perl-Scalar-List-Utils perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Taint-1.06-19.el8.x86_64.rpm \
          "

SRC_URI[perl-Test-Taint.sha256sum] = "1266e012ee66290c779a111195bf6f0f89b4b191464fb6cf39069aa78654e672"
