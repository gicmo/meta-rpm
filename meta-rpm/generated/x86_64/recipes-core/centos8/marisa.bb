SUMMARY = "generated recipe based on marisa srpm"
DESCRIPTION = "Description"
LICENSE = "BSD | LGPL-2.0"
RPM_LICENSE = "BSD or LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_marisa = "libmarisa.so.0"
RPM_SONAME_REQ_marisa = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_marisa = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/marisa-0.2.4-36.el8.x86_64.rpm \
          "

SRC_URI[marisa.sha256sum] = "bf3dc5f4b5155ec52275851668d2565a83169efcc2403b8342fe860de4bfa609"
