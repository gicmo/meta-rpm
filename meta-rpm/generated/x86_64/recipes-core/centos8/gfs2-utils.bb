SUMMARY = "generated recipe based on gfs2-utils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libblkid libuuid ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_gfs2-utils = "libblkid.so.1 libc.so.6 libncurses.so.6 libtinfo.so.6 libuuid.so.1 libz.so.1"
RDEPENDS_gfs2-utils = "bash glibc libblkid libuuid lvm2-lockd ncurses-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gfs2-utils-3.2.0-7.el8.x86_64.rpm \
          "

SRC_URI[gfs2-utils.sha256sum] = "63102804125a6563c4e2901cd3e7f1a2002611eebca5a5b4b2806e2ba6255031"
