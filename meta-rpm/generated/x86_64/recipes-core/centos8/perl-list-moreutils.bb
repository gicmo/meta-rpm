SUMMARY = "generated recipe based on perl-List-MoreUtils srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-List-MoreUtils = "perl-Carp perl-Exporter-Tiny perl-List-MoreUtils-XS perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-List-MoreUtils-0.428-2.el8.noarch.rpm \
          "

SRC_URI[perl-List-MoreUtils.sha256sum] = "b4f744bd61a1f9bea052275a48d94e8ee883907ce4f33798179124d07883ef0b"
