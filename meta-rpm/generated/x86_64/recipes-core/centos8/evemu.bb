SUMMARY = "generated recipe based on evemu srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libevdev pkgconfig-native"
RPM_SONAME_REQ_evemu = "libc.so.6 libevdev.so.2 libevemu.so.3"
RDEPENDS_evemu = "evemu-libs glibc libevdev"
RPM_SONAME_PROV_evemu-libs = "libevemu.so.3"
RPM_SONAME_REQ_evemu-libs = "libc.so.6 libevdev.so.2"
RDEPENDS_evemu-libs = "glibc libevdev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/evemu-2.7.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/evemu-libs-2.7.0-8.el8.x86_64.rpm \
          "

SRC_URI[evemu.sha256sum] = "e9bbc4f9e9902c973e32ce5bfd525f19eb283e15ee6473def287195d05e89a92"
SRC_URI[evemu-libs.sha256sum] = "51cf9647bddba885776a425443bd2e3d05fe8a5242ce3cb695d3cc11504136fe"
