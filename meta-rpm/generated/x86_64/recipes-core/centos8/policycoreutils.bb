SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs dbus-glib dbus-libs glib-2.0 libcap-ng libpcre libselinux libsemanage libsepol pam pkgconfig-native"
RPM_SONAME_REQ_policycoreutils = "libaudit.so.1 libc.so.6 libselinux.so.1 libsemanage.so.1 libsepol.so.1"
RDEPENDS_policycoreutils = "audit-libs bash coreutils diffutils gawk glibc grep libselinux libselinux-utils libsemanage libsepol rpm sed util-linux"
RDEPENDS_policycoreutils-dbus = "platform-python python3-policycoreutils python3-slip-dbus"
RPM_SONAME_REQ_policycoreutils-newrole = "libaudit.so.1 libc.so.6 libcap-ng.so.0 libpam.so.0 libpam_misc.so.0 libselinux.so.1"
RDEPENDS_policycoreutils-newrole = "audit-libs glibc libcap-ng libselinux pam policycoreutils"
RPM_SONAME_REQ_policycoreutils-restorecond = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libpcre.so.1 libselinux.so.1"
RDEPENDS_policycoreutils-restorecond = "bash dbus-glib dbus-libs glib2 glibc libselinux pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-2.9-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-dbus-2.9-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-newrole-2.9-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-restorecond-2.9-9.el8.x86_64.rpm \
          "

SRC_URI[policycoreutils.sha256sum] = "646258b615f7f12e8a25cdc27ae72703282475aab9c0e5cf51c457b2e346bded"
SRC_URI[policycoreutils-dbus.sha256sum] = "8e68ceb79f0bc7e77950e32fbad71e7db9ec047f10e35988e4e2dc7ccdd4a245"
SRC_URI[policycoreutils-newrole.sha256sum] = "ab3b29493906999275839c27eff42f2b7c37da7162182bb3a15f4ae56246629a"
SRC_URI[policycoreutils-restorecond.sha256sum] = "cce9a9928d68bbf763621c9346229d79df6774f86816746a91c0109ec7ae5b91"
