SUMMARY = "generated recipe based on perl-Perl-Destruct-Level srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Perl-Destruct-Level = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Perl-Destruct-Level = "glibc perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Perl-Destruct-Level-0.02-20.el8.x86_64.rpm \
          "

SRC_URI[perl-Perl-Destruct-Level.sha256sum] = "ee09b50982b29ad506f49fd3de46e1b40045a5a7e352fd2b37ed056b96ba9994"
