SUMMARY = "generated recipe based on ed srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GFDL-1.1"
RPM_LICENSE = "GPLv3+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ed = "libc.so.6"
RDEPENDS_ed = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ed-1.14.2-4.el8.x86_64.rpm \
          "

SRC_URI[ed.sha256sum] = "5164c6ac64ae78ff8ccbfd0fcdcd114663fa21ea43f535c7e0bda38e4d1c8a3e"
