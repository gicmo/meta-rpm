SUMMARY = "generated recipe based on perl-HTTP-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTTP-Tiny = "perl-Carp perl-Errno perl-IO perl-MIME-Base64 perl-Socket perl-Time-Local perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-HTTP-Tiny-0.074-1.el8.noarch.rpm \
          "

SRC_URI[perl-HTTP-Tiny.sha256sum] = "a1af93a1b62e8ca05b7597d5749a2b3d28735a86928f0432064fec61db1ff844"
