SUMMARY = "generated recipe based on perl-Text-CharWidth srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Text-CharWidth = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Text-CharWidth = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Text-CharWidth-0.04-32.el8.x86_64.rpm \
          "

SRC_URI[perl-Text-CharWidth.sha256sum] = "b00b114439227f94f23815069104a07e0194fd1898dea9b3f3033b4e59bf80ff"
