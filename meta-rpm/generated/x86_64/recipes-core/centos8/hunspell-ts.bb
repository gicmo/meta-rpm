SUMMARY = "generated recipe based on hunspell-ts srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ts = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ts-0.20110323.1-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-ts.sha256sum] = "884709c476dca8683bf5b93935e32655c3c13130db3ce71c32fba6ad3666fa1e"
