SUMMARY = "generated recipe based on hyphen-te srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-te = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-te-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-te.sha256sum] = "106ca49d7353c82532a98504ab2283d045254916202f7f019fef4d3ed8ee2289"
