SUMMARY = "generated recipe based on rpmlint srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_rpmlint = "binutils bzip2 cpio desktop-file-utils groff-base gzip perl-interpreter platform-python python3-rpm xz"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rpmlint-1.10-13.2.el8.noarch.rpm \
          "

SRC_URI[rpmlint.sha256sum] = "76f077ee9f7d01fc18901abb856333175d2adf9dc514696869ab80a2bd4bbea7"
