SUMMARY = "generated recipe based on mcelog srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mcelog = "libc.so.6"
RDEPENDS_mcelog = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mcelog-165-0.el8.x86_64.rpm \
          "

SRC_URI[mcelog.sha256sum] = "2a5e19fb87282d8774884999d03c48abbe83189bc1a87ef074b0f912e404bf75"
