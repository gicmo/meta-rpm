SUMMARY = "generated recipe based on mod_wsgi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-mod_wsgi = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1"
RDEPENDS_python3-mod_wsgi = "glibc httpd platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-mod_wsgi-4.6.4-4.el8.x86_64.rpm \
          "

SRC_URI[python3-mod_wsgi.sha256sum] = "e48240a67aaee4ac2474caa644523a5a48a8a1ab8164ca325f932bc6de9cda00"
