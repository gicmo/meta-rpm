SUMMARY = "generated recipe based on perl-JSON srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-JSON = "perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-JSON-2.97.001-2.el8.noarch.rpm \
          "

SRC_URI[perl-JSON.sha256sum] = "ecc0a0031bf49a067df76f5cc084a3a3dac82b969eb683b20c567dbb75b6b60f"
