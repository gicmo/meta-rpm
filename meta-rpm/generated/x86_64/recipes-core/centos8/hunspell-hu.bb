SUMMARY = "generated recipe based on hunspell-hu srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-2.0 | MPL-1.1"
RPM_LICENSE = "LGPLv2+ or GPLv2+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-hu = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-hu-1.6.1-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-hu.sha256sum] = "86addea1419dda3a2d619d691ce900f05b6620c6f41779f332122f5db7c0bbfb"
