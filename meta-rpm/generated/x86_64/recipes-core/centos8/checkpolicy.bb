SUMMARY = "generated recipe based on checkpolicy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_checkpolicy = "libc.so.6"
RDEPENDS_checkpolicy = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/checkpolicy-2.9-1.el8.x86_64.rpm \
          "

SRC_URI[checkpolicy.sha256sum] = "d5c283da0d2666742635754626263f6f78e273cd46d83d2d66ed43730a731685"
