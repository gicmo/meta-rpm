SUMMARY = "generated recipe based on dvd+rw-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_dvd+rw-tools = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_dvd+rw-tools = "glibc libgcc libstdc++ xorriso"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dvd+rw-tools-7.1-27.el8.x86_64.rpm \
          "

SRC_URI[dvd+rw-tools.sha256sum] = "ab94a374abcfb53e646806ddad099af4fce9c9340b4e0a4da9398aba7a74da3d"
