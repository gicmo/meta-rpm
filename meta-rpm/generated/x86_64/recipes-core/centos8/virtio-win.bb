SUMMARY = "generated recipe based on virtio-win srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & GPL-2.0"
RPM_LICENSE = "Red Hat Proprietary and GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/virtio-win-1.9.12-2.el8.noarch.rpm \
          "

SRC_URI[virtio-win.sha256sum] = "643030582388e87cf7ca84428b576774d44614a70cafc7e86b927937a12a9f86"
