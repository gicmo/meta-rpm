SUMMARY = "generated recipe based on mythes-hu srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & (GPL-2.0 | LGPL-2.0 | MPL-1.1) & GPL-2.0 & (GPL-2.0 | LGPL-2.0 | MPL-1.1)"
RPM_LICENSE = "GPLv2+ and (GPLv2+ or LGPLv2+ or MPLv1.1) and GPLv2 and (GPL+ or LGPLv2+ or MPLv1.1)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-hu = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-hu-0.20101019-15.el8.noarch.rpm \
          "

SRC_URI[mythes-hu.sha256sum] = "e69808c677f3ef4150abbcbdb8bf6917d3d42cccfeefc34645a6bf2d68e3bb2c"
