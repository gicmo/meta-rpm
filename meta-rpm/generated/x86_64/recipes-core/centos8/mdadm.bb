SUMMARY = "generated recipe based on mdadm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mdadm = "libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_mdadm = "bash chkconfig coreutils glibc libreport-filesystem systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mdadm-4.1-13.el8.x86_64.rpm \
          "

SRC_URI[mdadm.sha256sum] = "c0fdb7887239752074760d1aa3d8bc78a729e7def1c6e5a7a13b10bf727702fd"
