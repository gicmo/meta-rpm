SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-fr = "locale-base-fr-be (= 2.28) locale-base-fr-be.utf8 (= 2.28) locale-base-fr-be@euro (= 2.28) locale-base-fr-ca (= 2.28) locale-base-fr-ca.utf8 (= 2.28) locale-base-fr-ch (= 2.28) locale-base-fr-ch.utf8 (= 2.28) locale-base-fr-fr (= 2.28) locale-base-fr-fr.utf8 (= 2.28) locale-base-fr-fr@euro (= 2.28) locale-base-fr-lu (= 2.28) locale-base-fr-lu.utf8 (= 2.28) locale-base-fr-lu@euro (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr-be (= 2.28) virtual-locale-fr-be.utf8 (= 2.28) virtual-locale-fr-be@euro (= 2.28) virtual-locale-fr-ca (= 2.28) virtual-locale-fr-ca.utf8 (= 2.28) virtual-locale-fr-ch (= 2.28) virtual-locale-fr-ch.utf8 (= 2.28) virtual-locale-fr-fr (= 2.28) virtual-locale-fr-fr.utf8 (= 2.28) virtual-locale-fr-fr@euro (= 2.28) virtual-locale-fr-lu (= 2.28) virtual-locale-fr-lu.utf8 (= 2.28) virtual-locale-fr-lu@euro (= 2.28)"
RDEPENDS_glibc-langpack-fr = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-fr-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-fr.sha256sum] = "9bf737d642bbbf9bdb29996b7b947fe72b58a7d1f6556254390c379c0702d377"
