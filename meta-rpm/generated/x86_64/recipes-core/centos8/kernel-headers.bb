SUMMARY = "generated recipe based on kernel srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & CLOSED"
RPM_LICENSE = "GPLv2 and Redistributable, no modification permitted"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "linux-libc-headers"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-headers-4.18.0-193.28.1.el8_2.x86_64.rpm \
          "

SRC_URI[kernel-headers.sha256sum] = "ba7c1dc13a28f42796c238b0d9e3d5c5e85930aa3ddce0da7bbf0fb2464daa96"
