SUMMARY = "generated recipe based on mythes-nl srpm"
DESCRIPTION = "Description"
LICENSE = "BSD | CC-BY-1.0 "
RPM_LICENSE = "BSD or CC-BY"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-nl = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-nl-0.20130131-8.el8.noarch.rpm \
          "

SRC_URI[mythes-nl.sha256sum] = "542d87472ebf6f7ac5a11e98e56340531a2f97d3433df8977a0d0e9a3e5de280"
