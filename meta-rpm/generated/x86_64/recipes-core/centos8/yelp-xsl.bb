SUMMARY = "generated recipe based on yelp-xsl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/yelp-xsl-3.28.0-2.el8.noarch.rpm \
          "

SRC_URI[yelp-xsl.sha256sum] = "bb949bd5467f7c59fbb4690035f941f47facadd8bd5378ac931c98099d059e03"
