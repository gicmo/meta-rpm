SUMMARY = "generated recipe based on switcheroo-control srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_switcheroo-control = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_switcheroo-control = "bash glib2 glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/switcheroo-control-1.1-5.el8.x86_64.rpm \
          "

SRC_URI[switcheroo-control.sha256sum] = "364e5ea04e07856e75ebe702d9a66dfff47ad651cf8fa58b6f17971ba484b6e2"
