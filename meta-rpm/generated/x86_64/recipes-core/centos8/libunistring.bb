SUMMARY = "generated recipe based on libunistring srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-3.0"
RPM_LICENSE = "GPLv2+ or LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libunistring = "libunistring.so.2"
RPM_SONAME_REQ_libunistring = "libc.so.6"
RDEPENDS_libunistring = "glibc info"
RPM_SONAME_REQ_libunistring-devel = "libunistring.so.2"
RPROVIDES_libunistring-devel = "libunistring-dev (= 0.9.9)"
RDEPENDS_libunistring-devel = "bash libunistring"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libunistring-0.9.9-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libunistring-devel-0.9.9-3.el8.x86_64.rpm \
          "

SRC_URI[libunistring.sha256sum] = "20bb189228afa589141d9c9d4ed457729d13c11608305387602d0b00ed0a3093"
SRC_URI[libunistring-devel.sha256sum] = "91e687d9916aa62f521310f60f247e6a96118f0db25b397800c49eef4a31349d"
