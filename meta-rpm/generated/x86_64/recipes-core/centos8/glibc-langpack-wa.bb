SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-wa = "locale-base-wa-be (= 2.28) locale-base-wa-be.utf8 (= 2.28) locale-base-wa-be@euro (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa-be (= 2.28) virtual-locale-wa-be.utf8 (= 2.28) virtual-locale-wa-be@euro (= 2.28)"
RDEPENDS_glibc-langpack-wa = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-wa-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-wa.sha256sum] = "26c526c56ef3cac08561512c3b745957ff4a41e56ba0332a8e9ba3e8ef304502"
