SUMMARY = "generated recipe based on mariadb-connector-odbc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "mariadb-connector-c pkgconfig-native unixodbc"
RPM_SONAME_PROV_mariadb-connector-odbc = "libmaodbc.so"
RPM_SONAME_REQ_mariadb-connector-odbc = "libc.so.6 libm.so.6 libmariadb.so.3 libodbcinst.so.2"
RDEPENDS_mariadb-connector-odbc = "glibc mariadb-connector-c unixODBC"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mariadb-connector-odbc-3.0.7-1.el8.x86_64.rpm \
          "

SRC_URI[mariadb-connector-odbc.sha256sum] = "091569eaedae5ecac18a8228cdecf91fe56b46bc917089d389732404ae273017"
