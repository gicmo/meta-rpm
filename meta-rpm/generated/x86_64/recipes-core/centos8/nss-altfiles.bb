SUMMARY = "generated recipe based on nss-altfiles srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_nss-altfiles = "libnss_altfiles.so.2"
RPM_SONAME_REQ_nss-altfiles = "libpthread.so.0"
RDEPENDS_nss-altfiles = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-altfiles-2.18.1-12.el8.x86_64.rpm \
          "

SRC_URI[nss-altfiles.sha256sum] = "44a34b913d431a3b79f57a2dea23c23fbd10a1b6de30c571cde342e457a752e8"
