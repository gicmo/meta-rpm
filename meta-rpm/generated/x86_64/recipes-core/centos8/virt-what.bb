SUMMARY = "generated recipe based on virt-what srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_virt-what = "libc.so.6"
RDEPENDS_virt-what = "bash dmidecode glibc util-linux which"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/virt-what-1.18-6.el8.x86_64.rpm \
          "

SRC_URI[virt-what.sha256sum] = "4c02e3e1808f0189269b242242468a364007a8f12c6e38afc24b599b2848b0fa"
