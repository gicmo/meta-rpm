SUMMARY = "generated recipe based on rubygem-rspec-mocks srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-rspec-mocks = "rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rubygem-rspec-mocks-3.7.0-4.el8.noarch.rpm \
          "

SRC_URI[rubygem-rspec-mocks.sha256sum] = "6db7ce942cc1a9b0340f0bae44d50ae293f99838a51deff80f4839985e07db88"
