SUMMARY = "generated recipe based on symlinks srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Copyright only"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_symlinks = "libc.so.6"
RDEPENDS_symlinks = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/symlinks-1.4-19.el8.x86_64.rpm \
          "

SRC_URI[symlinks.sha256sum] = "0883d90cad753c96cda88da476d6ec8ebf925c9c255cee8527f9fa5443354292"
