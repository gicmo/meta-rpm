SUMMARY = "generated recipe based on libEMF srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libEMF = "libEMF.so.1"
RPM_SONAME_REQ_libEMF = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libEMF = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libEMF-devel = "libEMF.so.1"
RPROVIDES_libEMF-devel = "libEMF-dev (= 1.0.9)"
RDEPENDS_libEMF-devel = "libEMF libstdc++-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libEMF-1.0.9-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libEMF-devel-1.0.9-5.el8.x86_64.rpm \
          "

SRC_URI[libEMF.sha256sum] = "67af720b861526721d0ad97fcc615e6ed68b67b3346d315724215ec1667f3a96"
SRC_URI[libEMF-devel.sha256sum] = "dfd6171d08fd58715b9595fda6e1da1b95b9f128f17c81f3530d6b38900db4d0"
