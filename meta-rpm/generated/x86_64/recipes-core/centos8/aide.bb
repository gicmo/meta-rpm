SUMMARY = "generated recipe based on aide srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl attr audit-libs curl e2fsprogs libgcrypt libgpg-error libpcre libselinux pkgconfig-native zlib"
RPM_SONAME_REQ_aide = "libacl.so.1 libattr.so.1 libaudit.so.1 libc.so.6 libcurl.so.4 libe2p.so.2 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libpcre.so.1 libselinux.so.1 libz.so.1"
RDEPENDS_aide = "audit-libs e2fsprogs-libs glibc libacl libattr libcurl libgcrypt libgpg-error libselinux pcre zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/aide-0.16-11.el8.x86_64.rpm \
          "

SRC_URI[aide.sha256sum] = "9574a9d04494c68d9d2984070a7c465d85fa2d8e468dbfaaf20d6665ec99e66e"
