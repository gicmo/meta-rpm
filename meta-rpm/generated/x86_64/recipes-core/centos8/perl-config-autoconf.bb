SUMMARY = "generated recipe based on perl-Config-AutoConf srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Config-AutoConf = "perl-Capture-Tiny perl-Carp perl-Exporter perl-File-Temp perl-PathTools perl-Text-ParseWords perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Config-AutoConf-0.315-2.el8.noarch.rpm \
          "

SRC_URI[perl-Config-AutoConf.sha256sum] = "4317a7d70b556c03f57eea07b4fd350d6e2f863866db41eceebe8107ac7c5c3b"
