SUMMARY = "generated recipe based on psacct srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_psacct = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_psacct = "bash chkconfig coreutils glibc info systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/psacct-6.6.3-4.el8.aarch64.rpm \
          "

SRC_URI[psacct.sha256sum] = "d9bd5cbd70eb715511c99ec937152424a0785c7cca037ed160ede72f114506f0"
