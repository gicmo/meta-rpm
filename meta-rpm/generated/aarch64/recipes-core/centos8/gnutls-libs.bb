SUMMARY = "generated recipe based on gnutls srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & LGPL-2.0"
RPM_LICENSE = "GPLv3+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gmp libidn2 libtasn1 libunistring nettle p11-kit pkgconfig-native"
RPM_SONAME_PROV_gnutls = "libgnutls.so.30"
RPM_SONAME_REQ_gnutls = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 libhogweed.so.4 libidn2.so.0 libnettle.so.6 libp11-kit.so.0 libtasn1.so.6 libunistring.so.2"
RDEPENDS_gnutls = "crypto-policies glibc gmp libidn2 libtasn1 libunistring nettle p11-kit p11-kit-trust"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gnutls-3.6.8-11.el8_2.aarch64.rpm \
          "

SRC_URI[gnutls.sha256sum] = "aeab3873f101b77861a5b45983fe9e9183502863c6fde595beba9f6aa16a88f7"
