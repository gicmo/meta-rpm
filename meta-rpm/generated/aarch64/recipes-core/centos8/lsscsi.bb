SUMMARY = "generated recipe based on lsscsi srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lsscsi = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lsscsi = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lsscsi-0.30-1.el8.aarch64.rpm \
          "

SRC_URI[lsscsi.sha256sum] = "54a0cc194a5c31f4f45d1d2ccf9790e51529d86f0aaf083092096e09011d7bab"
