SUMMARY = "generated recipe based on perl-Net-Server srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Net-Server = "perl-Carp perl-Errno perl-Exporter perl-File-Temp perl-IO perl-IO-Multiplex perl-IO-Socket-INET6 perl-Scalar-List-Utils perl-Socket perl-Sys-Syslog perl-Time-HiRes perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Net-Server-2.009-3.el8.noarch.rpm \
          "

SRC_URI[perl-Net-Server.sha256sum] = "b9639f5d792eb747f52964cb3e7b664bd3dd9c7234bfbd608c5e68d92d72bc06"
