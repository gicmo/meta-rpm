SUMMARY = "generated recipe based on mythes-fr srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-fr = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-fr-2.3-10.el8.noarch.rpm \
          "

SRC_URI[mythes-fr.sha256sum] = "b8edad52339f1e21714bbed331f656af0edc809535d6351ff88b750a590fbb4d"
