SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-bg = "locale-base-bg-bg (= 2.28) locale-base-bg-bg.utf8 (= 2.28) virtual-locale-bg (= 2.28) virtual-locale-bg (= 2.28) virtual-locale-bg-bg (= 2.28) virtual-locale-bg-bg.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-bg = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bg-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-bg.sha256sum] = "78e7a8efb78e18c7a7635013552fb219b4f215a3a4e18d0f22a0f33712f4dc35"
