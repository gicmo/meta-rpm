SUMMARY = "generated recipe based on traceroute srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_traceroute = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_traceroute = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/traceroute-2.1.0-6.el8.aarch64.rpm \
          "

SRC_URI[traceroute.sha256sum] = "1914ffd356506dd43b92fd943594f3216d4c6416aae71c3863937978effc5f80"
