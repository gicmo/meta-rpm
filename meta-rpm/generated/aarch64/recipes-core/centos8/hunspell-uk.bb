SUMMARY = "generated recipe based on hunspell-uk srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0 | MPL-1.1"
RPM_LICENSE = "GPLv2+ or LGPLv2+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-uk = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-uk-1.8.0-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-uk.sha256sum] = "3bfb61544d45824ab090981895f592f1ae2b341921b180fea099c7b34f823076"
