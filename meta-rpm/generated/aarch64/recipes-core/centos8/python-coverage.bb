SUMMARY = "generated recipe based on python-coverage srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & MIT & (MIT | GPL-2.0)"
RPM_LICENSE = "ASL 2.0 and MIT and (MIT or GPL)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_platform-python-coverage = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_platform-python-coverage = "glibc platform-python platform-python-setuptools python3-libs"
RDEPENDS_python3-coverage = "bash platform-python platform-python-coverage python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/platform-python-coverage-4.5.1-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-coverage-4.5.1-7.el8.aarch64.rpm \
          "

SRC_URI[platform-python-coverage.sha256sum] = "87834f63d2cce6bdeba9b721cc9720dab5e3e673136621a9b5ae99674d60146b"
SRC_URI[python3-coverage.sha256sum] = "2fb543f1058d65978c8e2f283d2482b444cd96051d91cd306682611ac5a72bcc"
