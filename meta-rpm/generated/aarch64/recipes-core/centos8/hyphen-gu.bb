SUMMARY = "generated recipe based on hyphen-gu srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-gu = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-gu-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-gu.sha256sum] = "ba59ae519caeaa8e33a6727e5fe4e16a8e98527c8f1851c3364af904a3fb984b"
