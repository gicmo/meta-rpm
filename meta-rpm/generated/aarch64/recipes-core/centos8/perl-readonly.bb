SUMMARY = "generated recipe based on perl-Readonly srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Readonly = "perl-Carp perl-Exporter perl-Storable perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Readonly-2.05-5.el8.noarch.rpm \
          "

SRC_URI[perl-Readonly.sha256sum] = "638ba83911b1f4e1df6920c9044c0615d7f38e4e2fa51b3006b224df2618fd78"
