SUMMARY = "generated recipe based on pesign srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "efivar libuuid nspr nss pkgconfig-native popt"
RPM_SONAME_REQ_pesign = "libc.so.6 libdl.so.2 libefivar.so.1 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpopt.so.0 libpthread.so.0 libsmime3.so libssl3.so libuuid.so.1"
RDEPENDS_pesign = "bash efivar-libs glibc libuuid nspr nss nss-tools nss-util platform-python popt rpm shadow-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pesign-0.112-25.el8.aarch64.rpm \
          "

SRC_URI[pesign.sha256sum] = "45f58f6000b669fd8d513a3e0f7cc287cc1ec563f4a84c2ec2320210e6121c49"
