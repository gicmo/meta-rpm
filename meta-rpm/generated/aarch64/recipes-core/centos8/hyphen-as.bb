SUMMARY = "generated recipe based on hyphen-as srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-as = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-as-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-as.sha256sum] = "766549622da9d029255a53a424bb447558d6536a0fbc2e4f2af8144362ea328b"
