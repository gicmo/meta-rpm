SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-tt = "locale-base-tt-ru (= 2.28) locale-base-tt-ru@iqtelif (= 2.28) virtual-locale-tt (= 2.28) virtual-locale-tt (= 2.28) virtual-locale-tt-ru (= 2.28) virtual-locale-tt-ru@iqtelif (= 2.28)"
RDEPENDS_glibc-langpack-tt = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tt-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-tt.sha256sum] = "130da571de4b9b0c518a61ecd6082871d514531e6720bbca2070ce2a43f8eb69"
