SUMMARY = "generated recipe based on hunspell-ta srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ta = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ta-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-ta.sha256sum] = "9940550bf0ef90ed07ea6136871a8106461da4a4def69729c819680525004ad4"
