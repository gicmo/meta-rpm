SUMMARY = "generated recipe based on lua-socket srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lua-socket = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lua-socket = "glibc lua"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lua-socket-3.0-0.17.rc1.el8.aarch64.rpm \
          "

SRC_URI[lua-socket.sha256sum] = "c8568551dea0346b0ae421ed78d9a2d15c1a912b388b7f2cd6c2289e200c6b52"
