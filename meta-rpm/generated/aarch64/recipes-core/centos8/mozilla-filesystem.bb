SUMMARY = "generated recipe based on mozilla-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-1.1"
RPM_LICENSE = "MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mozilla-filesystem-1.9-18.el8.aarch64.rpm \
          "

SRC_URI[mozilla-filesystem.sha256sum] = "981cef426f95be024fb1ec6d8d99cffb04c99db3ac8005813b849c2402fc998a"
