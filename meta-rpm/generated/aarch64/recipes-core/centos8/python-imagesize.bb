SUMMARY = "generated recipe based on python-imagesize srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-imagesize = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-imagesize-1.0.0-2.el8.noarch.rpm \
          "

SRC_URI[python3-imagesize.sha256sum] = "70b26b6b3d17a10d2da975936b3d41cf85d7739f2b1921ba7073d777544d61a4"
