SUMMARY = "generated recipe based on rust-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rust-srpm-macros-5-2.el8.noarch.rpm \
          "

SRC_URI[rust-srpm-macros.sha256sum] = "c96962e21d09c28993b3a37c0c808e63486c3c06b1a22b749ca938b4bfaa2e7a"
