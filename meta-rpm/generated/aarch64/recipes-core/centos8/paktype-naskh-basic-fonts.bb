SUMMARY = "generated recipe based on paktype-naskh-basic-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_paktype-naskh-basic-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/paktype-naskh-basic-fonts-4.1-9.el8.noarch.rpm \
          "

SRC_URI[paktype-naskh-basic-fonts.sha256sum] = "6cee03f67ccfd6a2fe2ab4da633372d986ba28490b92d6976aff7c4e189d18c8"
