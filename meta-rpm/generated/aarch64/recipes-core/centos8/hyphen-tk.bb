SUMMARY = "generated recipe based on hyphen-tk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-tk = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-tk-0.20110620-12.el8.noarch.rpm \
          "

SRC_URI[hyphen-tk.sha256sum] = "bb5372fdc32eb55e0f76a45f0bff82e04e3958b4390735936a30f2a9308a88bd"
