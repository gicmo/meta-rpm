SUMMARY = "generated recipe based on perl-Convert-ASN1 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Convert-ASN1 = "perl-Carp perl-Encode perl-Exporter perl-Socket perl-Time-Local perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Convert-ASN1-0.27-17.el8.noarch.rpm \
          "

SRC_URI[perl-Convert-ASN1.sha256sum] = "0c68d888bff0b0456e79b043a2551ced7f4b78f6bc60ab2c08a120eb87e6e637"
