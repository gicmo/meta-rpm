SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-mt = "locale-base-mt-mt (= 2.28) locale-base-mt-mt.utf8 (= 2.28) virtual-locale-mt (= 2.28) virtual-locale-mt (= 2.28) virtual-locale-mt-mt (= 2.28) virtual-locale-mt-mt.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mt = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mt-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-mt.sha256sum] = "1d24b913e5a44121c4a18497526c5672df001692833499e0758ba588a8403210"
