SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-vi = "locale-base-vi-vn (= 2.28) virtual-locale-vi (= 2.28) virtual-locale-vi-vn (= 2.28)"
RDEPENDS_glibc-langpack-vi = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-vi-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-vi.sha256sum] = "41ea24f14ce7ccb2a029bd54d001acc8dd1c17ce6279a04447033ca4443b8543"
