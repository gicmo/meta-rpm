SUMMARY = "generated recipe based on testng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_testng = "beust-jcommander java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_testng-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/testng-6.14.3-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/testng-javadoc-6.14.3-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[testng.sha256sum] = "836a254c7b78f1a429fd52667f95516b3f18477856448611400edd9941aabaff"
SRC_URI[testng-javadoc.sha256sum] = "defbb70213be1abd137251616e433d8527f0a5789143eb2e80b0e933f795f68b"
