SUMMARY = "generated recipe based on nasm srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_nasm = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_nasm = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/nasm-2.13.03-2.el8.aarch64.rpm \
          "

SRC_URI[nasm.sha256sum] = "c86851482fadd8ffb50cf4ea568e7ef633bf3bac8c721d7b635cfd4a31f28b1b"
