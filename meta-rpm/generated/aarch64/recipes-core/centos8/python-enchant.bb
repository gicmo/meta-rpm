SUMMARY = "generated recipe based on python-enchant srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-enchant = "enchant platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-enchant-2.0.0-3.el8.noarch.rpm \
          "

SRC_URI[python3-enchant.sha256sum] = "8c8a449c013b2dae070c7c5f3e6d33b18016b57ed5e54d9858dabc6c1f58bd07"
