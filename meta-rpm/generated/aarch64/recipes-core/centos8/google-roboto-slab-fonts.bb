SUMMARY = "generated recipe based on google-roboto-slab-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/google-roboto-slab-fonts-1.100263-0.7.20150923git.el8.noarch.rpm \
          "

SRC_URI[google-roboto-slab-fonts.sha256sum] = "fb83524a608bbbd6d36bc7656980f8cddbb12888add12db473bf74ec0a68bea9"
