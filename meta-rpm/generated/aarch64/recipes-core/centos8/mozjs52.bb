SUMMARY = "generated recipe based on mozjs52 srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-2.0 & MPL-1.1 & BSD & GPL-2.0 & GPL-3.0 & LGPL-2.0.1 & LGPL-2.0.1+ & AFL-1.2 & CLOSED"
RPM_LICENSE = "MPLv2.0 and MPLv1.1 and BSD and GPLv2+ and GPLv3+ and LGPLv2.1 and LGPLv2.1+ and AFL and ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native readline zlib"
RPM_SONAME_PROV_mozjs52 = "libmozjs-52.so.0"
RPM_SONAME_REQ_mozjs52 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mozjs52 = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_mozjs52-devel = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libmozjs-52.so.0 libpthread.so.0 libreadline.so.7 libstdc++.so.6 libz.so.1"
RPROVIDES_mozjs52-devel = "mozjs52-dev (= 52.9.0)"
RDEPENDS_mozjs52-devel = "glibc libgcc libstdc++ mozjs52 pkgconf-pkg-config readline zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mozjs52-52.9.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mozjs52-devel-52.9.0-2.el8.aarch64.rpm \
          "

SRC_URI[mozjs52.sha256sum] = "7892e10c79e3f050ef813f72529030976f43b0a8f0d86262c47e1e197215b5b6"
SRC_URI[mozjs52-devel.sha256sum] = "d1523d25da562b5a1a89943cc91f76599309000ae3f17ee2a8d6f08c943e15a3"
