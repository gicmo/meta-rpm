SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-ms = "locale-base-ms-my (= 2.28) locale-base-ms-my.utf8 (= 2.28) virtual-locale-ms (= 2.28) virtual-locale-ms (= 2.28) virtual-locale-ms-my (= 2.28) virtual-locale-ms-my.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ms = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ms-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-ms.sha256sum] = "539a5a600c840a677bb1be6c68b9d5a2b98a495782c1b812d8cba7b71a541d0b"
