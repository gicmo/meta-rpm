SUMMARY = "generated recipe based on lmdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "OpenLDAP"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lmdb-libs = "liblmdb.so.0.0.0"
RPM_SONAME_REQ_lmdb-libs = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_lmdb-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lmdb-libs-0.9.23-5.el8.aarch64.rpm \
          "

SRC_URI[lmdb-libs.sha256sum] = "8958304ace19f71777842c64bea75805e0064ec59a004698e20b239c0f2ce1a8"
