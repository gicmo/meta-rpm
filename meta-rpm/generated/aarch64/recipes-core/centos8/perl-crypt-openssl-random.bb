SUMMARY = "generated recipe based on perl-Crypt-OpenSSL-Random srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Crypt-OpenSSL-Random = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0 libssl.so.1.1"
RDEPENDS_perl-Crypt-OpenSSL-Random = "glibc openssl-libs perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Crypt-OpenSSL-Random-0.15-3.el8.aarch64.rpm \
          "

SRC_URI[perl-Crypt-OpenSSL-Random.sha256sum] = "b1b89067974e95d13f506d51b92d7bb215b578d48d122bf1b84eae37095fa1f7"
