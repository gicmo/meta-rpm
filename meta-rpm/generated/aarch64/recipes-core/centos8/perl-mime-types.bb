SUMMARY = "generated recipe based on perl-MIME-Types srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-MIME-Types = "perl-Carp perl-Exporter perl-PathTools perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-MIME-Types-2.17-3.el8.noarch.rpm \
          "

SRC_URI[perl-MIME-Types.sha256sum] = "d04234bba83ee19c3634d200ae0fca2a259d1e1a4d4f8704902219982443afe8"
