SUMMARY = "generated recipe based on radvd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "BSD with advertising"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_radvd = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_radvd = "bash glibc shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/radvd-2.17-14.el8.aarch64.rpm \
          "

SRC_URI[radvd.sha256sum] = "68948a470c665328f3db4d9dc14f4fa06e372ebc37766c3aca12ab1bf6e78f48"
