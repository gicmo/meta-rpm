SUMMARY = "generated recipe based on dosfstools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_dosfstools = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_dosfstools = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dosfstools-4.1-6.el8.aarch64.rpm \
          "

SRC_URI[dosfstools.sha256sum] = "e57a218c73df587fb441a22bd4e5f97afb8cbe8812707b26b6dd658910e52dcc"
