SUMMARY = "generated recipe based on hunspell-ar srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0 | MPL-1.1"
RPM_LICENSE = "GPLv2 or LGPLv2 or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ar = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ar-3.5-7.el8.noarch.rpm \
          "

SRC_URI[hunspell-ar.sha256sum] = "439cb607e3a6feb376f77f7105d90ecf6278bc42598d03a8c863340c3dd9c03b"
