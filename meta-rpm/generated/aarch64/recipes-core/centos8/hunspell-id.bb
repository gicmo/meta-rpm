SUMMARY = "generated recipe based on hunspell-id srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-id = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-id-0.20040812-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-id.sha256sum] = "d213c2077c54ccc0b172541738d593f6827a62a97abe555b71ac280183846c2b"
