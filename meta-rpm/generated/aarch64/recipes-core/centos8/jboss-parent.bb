SUMMARY = "generated recipe based on jboss-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CC0-1.0"
RPM_LICENSE = "CC0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jboss-parent = "java-1.8.0-openjdk-headless javapackages-filesystem maven-source-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jboss-parent-20-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jboss-parent.sha256sum] = "1d53cb92d1500fdb9d9ef66b3689a054829c57ba0a442aba9850f89a480c1309"
