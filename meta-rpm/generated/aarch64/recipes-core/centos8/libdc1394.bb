SUMMARY = "generated recipe based on libdc1394 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libraw1394 libusb1 pkgconfig-native"
RPM_SONAME_PROV_libdc1394 = "libdc1394.so.22"
RPM_SONAME_REQ_libdc1394 = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libraw1394.so.11 libusb-1.0.so.0"
RDEPENDS_libdc1394 = "glibc libraw1394 libusbx"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdc1394-2.2.2-10.el8.aarch64.rpm \
          "

SRC_URI[libdc1394.sha256sum] = "9dec00cab5b27a9e85a31c4ee601d32646c137079e7ca88ea1ecc0702fac017c"
