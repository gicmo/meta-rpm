SUMMARY = "generated recipe based on centos-release srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_centos-release = "bash centos-repos"
RDEPENDS_centos-repos = "centos-gpg-keys centos-release"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/centos-gpg-keys-8.2-2.2004.0.2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/centos-release-8.2-2.2004.0.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/centos-repos-8.2-2.2004.0.2.el8.aarch64.rpm \
          "

SRC_URI[centos-gpg-keys.sha256sum] = "5678556b2344231639ffaa7f43563237dae3862f5652d23ce282f71637ec7255"
SRC_URI[centos-release.sha256sum] = "3328dde2e09fd9fcf65cb3a288871d86556a8e118e9304be5b09ed8333ebea30"
SRC_URI[centos-repos.sha256sum] = "3e3ad52e19ef98c3bd810fe82c40e650f639bd9e5ee069658a3c4b038f95c382"
