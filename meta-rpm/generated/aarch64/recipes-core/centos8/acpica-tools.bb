SUMMARY = "generated recipe based on acpica-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_acpica-tools = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_acpica-tools = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/acpica-tools-20180629-3.el8.aarch64.rpm \
          "

SRC_URI[acpica-tools.sha256sum] = "884154e73f8f96d08c50d64c4f2d8ec50b0283623e42a532ff03d4fea0652bb3"
