SUMMARY = "generated recipe based on gzip srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GFDL-1.1"
RPM_LICENSE = "GPLv3+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_gzip = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_gzip = "bash coreutils glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gzip-1.9-9.el8.aarch64.rpm \
          "

SRC_URI[gzip.sha256sum] = "88ecbc25e066f0dbd0cde2142c5e1fa27a68b349b66f0ee638086c03c96d1dc6"
