SUMMARY = "generated recipe based on wpa_supplicant srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-libs libnl openssl pkgconfig-native"
RPM_SONAME_REQ_wpa_supplicant = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdbus-1.so.3 libdl.so.2 libnl-3.so.200 libnl-genl-3.so.200 libnl-route-3.so.200 librt.so.1 libssl.so.1.1"
RDEPENDS_wpa_supplicant = "bash dbus-libs glibc libnl3 openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/wpa_supplicant-2.9-2.el8.aarch64.rpm \
          "

SRC_URI[wpa_supplicant.sha256sum] = "707dc6945601f3be3a75a2d51fcceaa3b0f3e860cbbe3bf9040540fd751b4419"
