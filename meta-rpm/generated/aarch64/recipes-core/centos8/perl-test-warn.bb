SUMMARY = "generated recipe based on perl-Test-Warn srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Warn = "perl-Carp perl-Exporter perl-Sub-Uplevel perl-Test-Simple perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Test-Warn-0.32-5.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Warn.sha256sum] = "13c0387926bc39ccaeef9e3f976cafb67e02d0c544c6058824cb7281b4d76884"
