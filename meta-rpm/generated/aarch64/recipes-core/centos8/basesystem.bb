SUMMARY = "generated recipe based on basesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_basesystem = "filesystem setup"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/basesystem-11-5.el8.noarch.rpm \
          "

SRC_URI[basesystem.sha256sum] = "48226934763e4c412c1eb65df314e6879720b4b1ebcb3d07c126c9526639cb68"
