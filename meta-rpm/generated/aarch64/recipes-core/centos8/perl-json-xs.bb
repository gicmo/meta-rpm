SUMMARY = "generated recipe based on perl-JSON-XS srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-JSON-XS = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-JSON-XS = "glibc perl-Encode perl-Exporter perl-Getopt-Long perl-Storable perl-Types-Serialiser perl-common-sense perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-JSON-XS-3.04-3.el8.aarch64.rpm \
          "

SRC_URI[perl-JSON-XS.sha256sum] = "ede9ff9ada6f2ac4767629ab08bb6696b0919e2064a293fe110b181117ac0b0d"
