SUMMARY = "generated recipe based on dialog srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_PROV_dialog = "libdialog.so.14"
RPM_SONAME_REQ_dialog = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_dialog = "glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dialog-1.3-13.20171209.el8.aarch64.rpm \
          "

SRC_URI[dialog.sha256sum] = "eebb16e5fe618541c1dbfe9c4277fde0355658dd2e6eb0505b2a52ca0b636015"
