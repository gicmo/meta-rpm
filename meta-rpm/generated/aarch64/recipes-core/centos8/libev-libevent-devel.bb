SUMMARY = "generated recipe based on libev srpm"
DESCRIPTION = "Description"
LICENSE = "BSD | GPL-2.0"
RPM_LICENSE = "BSD or GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libev pkgconfig-native"
RPROVIDES_libev-libevent-devel = "libev-libevent-dev (= 4.24)"
RDEPENDS_libev-libevent-devel = "libev-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libev-libevent-devel-4.24-6.el8.aarch64.rpm \
          "

SRC_URI[libev-libevent-devel.sha256sum] = "f2a4eb595110c25bd56b9ba7975c780939104b6e0b0f0acea303c1632b635724"
