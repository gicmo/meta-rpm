SUMMARY = "generated recipe based on PyYAML srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libyaml pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-pyyaml = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libyaml-0.so.2"
RDEPENDS_python3-pyyaml = "glibc libyaml platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-pyyaml-3.12-12.el8.aarch64.rpm \
          "

SRC_URI[python3-pyyaml.sha256sum] = "125e3be7258821f7bc210b7eee8591289ea4ce97edea2832d8e6a89f1b6969e5"
