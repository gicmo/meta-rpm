SUMMARY = "generated recipe based on hunspell-fy srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-fy = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-fy-3.0.0-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-fy.sha256sum] = "3cc8154e7614d9ac752d2f19e2d9a376fa9ad79f799218c7cc700e2cde245ac2"
