SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-br = "locale-base-br-fr (= 2.28) locale-base-br-fr.utf8 (= 2.28) locale-base-br-fr@euro (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br-fr (= 2.28) virtual-locale-br-fr.utf8 (= 2.28) virtual-locale-br-fr@euro (= 2.28)"
RDEPENDS_glibc-langpack-br = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-br-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-br.sha256sum] = "7e420bb6a30ac9428edd0f0bb158af0b3f6f24943bff229f0cc66629350c326d"
