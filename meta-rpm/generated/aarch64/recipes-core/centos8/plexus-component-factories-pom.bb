SUMMARY = "generated recipe based on plexus-component-factories-pom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-component-factories-pom = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-container-default plexus-pom"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-component-factories-pom-1.0-0.15.alpha11.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-component-factories-pom.sha256sum] = "f45fd775680cd58beab5318f18b2a742ae3c49508bfc3cdf9a195ec34820b0cb"
