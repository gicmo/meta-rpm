SUMMARY = "generated recipe based on ansible-freeipa srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_ansible-freeipa = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ansible-freeipa-0.1.8-3.el8.noarch.rpm \
          "

SRC_URI[ansible-freeipa.sha256sum] = "79d4db895c561faee3cdd69ee8a583a74e95a7144ce3dbf452399dd025a00f01"
