SUMMARY = "generated recipe based on perl-DynaLoader-Functions srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DynaLoader-Functions = "perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-DynaLoader-Functions-0.003-2.el8.noarch.rpm \
          "

SRC_URI[perl-DynaLoader-Functions.sha256sum] = "343af8f87e4cef0158ef71760aecbaef385ee9eae324ba679f1e7347698abff0"
