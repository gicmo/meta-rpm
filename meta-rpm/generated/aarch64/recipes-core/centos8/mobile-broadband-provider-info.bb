SUMMARY = "generated recipe based on mobile-broadband-provider-info srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mobile-broadband-provider-info-20190618-2.el8.noarch.rpm \
          "

SRC_URI[mobile-broadband-provider-info.sha256sum] = "e16983240ccdedef9e7084edbbee91fe79adc56a9649e13e567e28815506e8ac"
