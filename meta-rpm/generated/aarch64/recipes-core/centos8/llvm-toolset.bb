SUMMARY = "generated recipe based on llvm-toolset srpm"
DESCRIPTION = "Description"
LICENSE = "NCSA"
RPM_LICENSE = "NCSA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_llvm-toolset = "clang lld lldb llvm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/llvm-toolset-9.0.1-1.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
          "

SRC_URI[llvm-toolset.sha256sum] = "87c3400c232f693bd8db90891c617ad0d5f71c8cec392b1958bc1823490d4e2e"
