SUMMARY = "generated recipe based on jboss-jaxrs-2.0-api srpm"
DESCRIPTION = "Description"
LICENSE = "(CDDL-1.0 | GPL-2.0) & CLOSED"
RPM_LICENSE = "(CDDL or GPLv2 with exceptions) and ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jboss-jaxrs-2.0-api = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jboss-jaxrs-2.0-api-1.0.0-6.el8.noarch.rpm \
          "

SRC_URI[jboss-jaxrs-2.0-api.sha256sum] = "81dbece199f8ef5bac1e3baaf1cbf588aa349cdd9cda0a67ce86c0d720580a72"
