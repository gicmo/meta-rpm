SUMMARY = "generated recipe based on jq srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & CLOSED & CC-BY-1.0  & GPL-3.0"
RPM_LICENSE = "MIT and ASL 2.0 and CC-BY and GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "oniguruma pkgconfig-native"
RPM_SONAME_PROV_jq = "libjq.so.1"
RPM_SONAME_REQ_jq = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libonig.so.5"
RDEPENDS_jq = "glibc oniguruma"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jq-1.5-12.el8.aarch64.rpm \
          "

SRC_URI[jq.sha256sum] = "a0d9785deff96688b1890dab35b7b5505142a38a2d0c88b362e4d3ebb71e2f14"
