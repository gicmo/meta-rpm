SUMMARY = "generated recipe based on tagsoup srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & (GPL-2.0 | AFL-1.2)"
RPM_LICENSE = "ASL 2.0 and (GPLv2+ or AFL)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_tagsoup = "java-1.8.0-openjdk-headless javapackages-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tagsoup-1.2.1-15.el8.noarch.rpm \
          "

SRC_URI[tagsoup.sha256sum] = "37287df7bbcfeb40ffe80e2751474aeb4cdbefdbce898b86138097d3936119aa"
