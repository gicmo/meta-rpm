SUMMARY = "generated recipe based on lynx srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses openssl pkgconfig-native zlib"
RPM_SONAME_REQ_lynx = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libncursesw.so.6 libssl.so.1.1 libtinfo.so.6 libz.so.1"
RDEPENDS_lynx = "centos-indexhtml glibc ncurses-libs openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lynx-2.8.9-2.el8.aarch64.rpm \
          "

SRC_URI[lynx.sha256sum] = "5335d7d2d0ff93102457a0630deefb67e09746c88b71c6086baf18eac3b14d63"
