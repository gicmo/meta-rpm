SUMMARY = "generated recipe based on jboss-annotations-1.2-api srpm"
DESCRIPTION = "Description"
LICENSE = "CDDL-1.0 | GPL-2.0"
RPM_LICENSE = "CDDL or GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jboss-annotations-1.2-api = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jboss-annotations-1.2-api-1.0.0-4.el8.noarch.rpm \
          "

SRC_URI[jboss-annotations-1.2-api.sha256sum] = "77bf48ee348a830170a683328160914698d2e89ccf0bc7edc0e75bb787a9292f"
