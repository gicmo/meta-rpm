SUMMARY = "generated recipe based on gnome-getting-started-docs srpm"
DESCRIPTION = "Description"
LICENSE = "CC-BY-SA-1.0"
RPM_LICENSE = "CC-BY-SA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_gnome-getting-started-docs = "gnome-user-docs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-getting-started-docs-3.28.2-1.el8.noarch.rpm \
          "

SRC_URI[gnome-getting-started-docs.sha256sum] = "cbcf8dd9e3c8a142a5443f2201c37506dbf19b20f308585cb1205f6c66743afd"
