SUMMARY = "generated recipe based on abattis-cantarell-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_abattis-cantarell-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abattis-cantarell-fonts-0.0.25-4.el8.noarch.rpm \
          "

SRC_URI[abattis-cantarell-fonts.sha256sum] = "923b2b30ab34bc765fb6d563cacd09d3fdeece9b03a719b8bdcd1b4fd729365c"
