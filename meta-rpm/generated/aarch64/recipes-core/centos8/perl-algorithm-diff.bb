SUMMARY = "generated recipe based on perl-Algorithm-Diff srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Algorithm-Diff = "perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Algorithm-Diff-1.1903-9.el8.noarch.rpm \
          "

SRC_URI[perl-Algorithm-Diff.sha256sum] = "c417a8ddc01dd1ca4b58cca6b1224c7c9234a9f879985384496277ae0c711d65"
