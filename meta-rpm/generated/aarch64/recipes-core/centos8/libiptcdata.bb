SUMMARY = "generated recipe based on libiptcdata srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libiptcdata = "libiptcdata.so.0"
RPM_SONAME_REQ_libiptcdata = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libiptcdata = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libiptcdata-1.0.4-21.el8.aarch64.rpm \
          "

SRC_URI[libiptcdata.sha256sum] = "aa73884b2fb465d568023515b98973d7d8b8233e92d00c10a75bad359abb2924"
