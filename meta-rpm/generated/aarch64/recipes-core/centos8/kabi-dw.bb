SUMMARY = "generated recipe based on kabi-dw srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_kabi-dw = "ld-linux-aarch64.so.1 libc.so.6 libdw.so.1 libelf.so.1"
RDEPENDS_kabi-dw = "elfutils-libelf elfutils-libs glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kabi-dw-0-0.8.20181112git6fbd644.el8.aarch64.rpm \
          "

SRC_URI[kabi-dw.sha256sum] = "408d22231fd006f303616ba5ef7ea9b3bff24b037c79b9c3c3d8a4f0ee3c0b98"
