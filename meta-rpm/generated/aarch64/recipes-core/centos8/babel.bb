SUMMARY = "generated recipe based on babel srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-babel = "platform-python platform-python-setuptools python3-pytz"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-babel-2.5.1-5.el8.noarch.rpm \
          "

SRC_URI[python3-babel.sha256sum] = "9dbf3ceb7de5a727f1a36edd5add73dcd96c83f24afc81d78e254b518551da96"
