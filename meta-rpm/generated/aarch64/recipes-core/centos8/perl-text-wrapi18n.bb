SUMMARY = "generated recipe based on perl-Text-WrapI18N srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-WrapI18N = "perl-Exporter perl-Text-CharWidth perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Text-WrapI18N-0.06-30.el8.noarch.rpm \
          "

SRC_URI[perl-Text-WrapI18N.sha256sum] = "5da416fb8567fa4c49d54805e58338f2194995a01780983dbd707a77a68191fe"
