SUMMARY = "generated recipe based on hunspell-ak srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ak = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ak-0.9.1-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-ak.sha256sum] = "6c30db9290078fbff864d776c73c876807562133ac5e490bca31f16336da13b4"
