SUMMARY = "generated recipe based on hunspell-pt srpm"
DESCRIPTION = "Description"
LICENSE = "((LGPL-3.0 | MPL-1.0) & LGPL-2.0) & (GPL-2.0 | LGPL-2.0 | MPL-1.1)"
RPM_LICENSE = "((LGPLv3 or MPL) and LGPLv2) and (GPLv2 or LGPLv2 or MPLv1.1)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-pt = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-pt-0.20130125-10.el8.noarch.rpm \
          "

SRC_URI[hunspell-pt.sha256sum] = "899e56ec05a5ddfb72b55c7af6cdb65ec77037c61eb33dc07db1cca1e7d09ce8"
