SUMMARY = "generated recipe based on hunspell-br srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-br = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-br-0.15-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-br.sha256sum] = "ba895f64cd274016caab18df8005f0ecee9dc10ba56fb0987caeab8f4c2144de"
