SUMMARY = "generated recipe based on hyphen-grc srpm"
DESCRIPTION = "Description"
LICENSE = "LPPL-1.0"
RPM_LICENSE = "LPPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-grc = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-grc-0.20110913-12.el8.noarch.rpm \
          "

SRC_URI[hyphen-grc.sha256sum] = "bb630d9d246162dda50d53330965ac16c607de71dc743797fbb5106e3ab3783b"
