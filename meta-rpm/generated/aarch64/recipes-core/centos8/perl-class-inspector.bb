SUMMARY = "generated recipe based on perl-Class-Inspector srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Inspector = "perl-Exporter perl-PathTools perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Class-Inspector-1.32-2.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Inspector.sha256sum] = "24b9a381c389c94514357feee5367002456dfef44163aac27c99400e91c9da0b"
