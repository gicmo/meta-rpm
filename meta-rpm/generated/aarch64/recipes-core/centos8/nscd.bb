SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs libcap libselinux pkgconfig-native"
RPM_SONAME_REQ_nscd = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcap.so.2 libpthread.so.0 libselinux.so.1"
RDEPENDS_nscd = "audit-libs bash coreutils glibc libcap libselinux shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nscd-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[nscd.sha256sum] = "db9f1c9e0c221b0194946df3599c1cc2d41f4bdb9166f29c179ab3273a282030"
