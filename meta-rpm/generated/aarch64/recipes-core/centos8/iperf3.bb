SUMMARY = "generated recipe based on iperf3 srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_iperf3 = "libiperf.so.0"
RPM_SONAME_REQ_iperf3 = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_iperf3 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/iperf3-3.5-3.el8.aarch64.rpm \
          "

SRC_URI[iperf3.sha256sum] = "06ecfb3e3bef725de346a2f7dcf2872d2829677aa1756119a3f446f69f569e08"
