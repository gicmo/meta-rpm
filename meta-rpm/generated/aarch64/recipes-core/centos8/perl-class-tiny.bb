SUMMARY = "generated recipe based on perl-Class-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Tiny = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Class-Tiny-1.006-6.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Tiny.sha256sum] = "176b5fcffb752fd689ec786b856f7f8c6eebd57012bbccc99e3bb42711e2a101"
