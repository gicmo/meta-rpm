SUMMARY = "generated recipe based on perl-Sys-CPU srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & (LGPL-3.0 | Artistic-2.0)"
RPM_LICENSE = "(GPL+ or Artistic) and (LGPLv3 or Artistic 2.0)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-CPU = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-CPU = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Sys-CPU-0.61-14.el8.aarch64.rpm \
          "

SRC_URI[perl-Sys-CPU.sha256sum] = "29b09f41b37e3f684e5344246197f6d035c77cb9c0530c6582e51cd2cb60aeeb"
