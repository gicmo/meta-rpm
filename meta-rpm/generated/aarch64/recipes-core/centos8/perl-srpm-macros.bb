SUMMARY = "generated recipe based on perl-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-srpm-macros-1-25.el8.noarch.rpm \
          "

SRC_URI[perl-srpm-macros.sha256sum] = "486f391da85c02eaffba41bb2d661e3af52c07ed6b614ec1ebb1eb192dd78784"
