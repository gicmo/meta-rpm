SUMMARY = "generated recipe based on checkpolicy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_checkpolicy = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_checkpolicy = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/checkpolicy-2.9-1.el8.aarch64.rpm \
          "

SRC_URI[checkpolicy.sha256sum] = "01b89be34e48d345ba14a3856bba0d1ff94e79798b5f7529a6a0803b97adca15"
