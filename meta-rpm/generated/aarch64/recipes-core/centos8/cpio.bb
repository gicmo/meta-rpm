SUMMARY = "generated recipe based on cpio srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cpio = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_cpio = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cpio-2.12-8.el8.aarch64.rpm \
          "

SRC_URI[cpio.sha256sum] = "9a200f1cabd5869c7cd6483dc6e143e394bbd83fc6d653d279bc3ec25e1e35cc"
