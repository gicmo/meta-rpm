SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native selinux-policy-devel"
RPM_SONAME_REQ_policycoreutils-devel = "ld-linux-aarch64.so.1 libc.so.6"
RPROVIDES_policycoreutils-devel = "policycoreutils-dev (= 2.9)"
RDEPENDS_policycoreutils-devel = "dnf glibc make platform-python policycoreutils-python-utils selinux-policy-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/policycoreutils-devel-2.9-9.el8.aarch64.rpm \
          "

SRC_URI[policycoreutils-devel.sha256sum] = "bb411f7fffd4a2b3404f5222fe4d66bcfb0a3a03cb296939e83d9a9e30939c81"
