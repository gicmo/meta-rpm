SUMMARY = "generated recipe based on perl-IPC-Run3 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0 | BSD"
RPM_LICENSE = "GPL+ or Artistic or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IPC-Run3 = "perl-Carp perl-Exporter perl-File-Temp perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-IPC-Run3-0.048-12.el8.noarch.rpm \
          "

SRC_URI[perl-IPC-Run3.sha256sum] = "ea165c017caaf756264d4e04de27bd680b4178da20fe5076ae824b38ca694911"
