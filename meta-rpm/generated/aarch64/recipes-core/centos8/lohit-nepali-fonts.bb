SUMMARY = "generated recipe based on lohit-nepali-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-nepali-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lohit-nepali-fonts-2.94.2-3.el8.noarch.rpm \
          "

SRC_URI[lohit-nepali-fonts.sha256sum] = "1bac8377bd1a492ee8beaf7d22381a7e084263cf74f2f69813d3537deb24103d"
