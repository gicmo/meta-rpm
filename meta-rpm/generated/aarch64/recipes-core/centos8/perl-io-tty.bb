SUMMARY = "generated recipe based on perl-IO-Tty srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & BSD"
RPM_LICENSE = "(GPL+ or Artistic) and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-IO-Tty = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0 libutil.so.1"
RDEPENDS_perl-IO-Tty = "glibc perl-Carp perl-Exporter perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-IO-Tty-1.12-11.el8.aarch64.rpm \
          "

SRC_URI[perl-IO-Tty.sha256sum] = "17cdc2f339559a7094e1813b90e4e2a9e245a62fb1dbe3de0a31edeb9778cbd6"
