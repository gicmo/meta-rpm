SUMMARY = "generated recipe based on perl-Class-XSAccessor srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Class-XSAccessor = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Class-XSAccessor = "glibc perl-Carp perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Class-XSAccessor-1.19-14.el8.aarch64.rpm \
          "

SRC_URI[perl-Class-XSAccessor.sha256sum] = "251b8d77e27aeeb2fa4e37e26e41a10c13e8838862af7313850f14657816faf7"
