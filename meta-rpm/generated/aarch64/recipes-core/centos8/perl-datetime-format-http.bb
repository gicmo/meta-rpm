SUMMARY = "generated recipe based on perl-DateTime-Format-HTTP srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-Format-HTTP = "perl-DateTime perl-HTTP-Date perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-DateTime-Format-HTTP-0.42-9.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-Format-HTTP.sha256sum] = "47e62522c28286aeacb8a46400c896ad76597053a8bf21a30ee8974aac368582"
