SUMMARY = "generated recipe based on xdg-utils srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_xdg-utils = "bash coreutils desktop-file-utils which"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xdg-utils-1.1.2-5.el8.noarch.rpm \
          "

SRC_URI[xdg-utils.sha256sum] = "c097fbbedd0a4946f6314a835cbfa53993e77109edc589c23d8ca0997490abbf"
