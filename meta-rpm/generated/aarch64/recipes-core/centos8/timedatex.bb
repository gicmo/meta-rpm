SUMMARY = "generated recipe based on timedatex srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libselinux pkgconfig-native"
RPM_SONAME_REQ_timedatex = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libselinux.so.1"
RDEPENDS_timedatex = "bash glib2 glibc libselinux polkit systemd util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/timedatex-0.5-3.el8.aarch64.rpm \
          "

SRC_URI[timedatex.sha256sum] = "f6078b759fafad2208d3e572cec9fbfb473dea334bc7a90583811f451d493cef"
