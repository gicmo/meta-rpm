SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_policycoreutils-python-utils = "platform-python python3-policycoreutils"
RDEPENDS_python3-policycoreutils = "checkpolicy platform-python policycoreutils python3-audit python3-libselinux python3-libsemanage python3-setools"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/policycoreutils-python-utils-2.9-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-policycoreutils-2.9-9.el8.noarch.rpm \
          "

SRC_URI[policycoreutils-python-utils.sha256sum] = "f1b53326dec969c70f4c61da7241474ff7428d35d235301836ad1c0ce3031cab"
SRC_URI[python3-policycoreutils.sha256sum] = "f5b2e42438721527f01405474819f3346eb470315371086bc5aedcb46d34633e"
