SUMMARY = "generated recipe based on libksba srpm"
DESCRIPTION = "Description"
LICENSE = "(LGPL-3.0 | GPL-2.0) & GPL-3.0"
RPM_LICENSE = "(LGPLv3+ or GPLv2+) and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgpg-error pkgconfig-native"
RPM_SONAME_PROV_libksba = "libksba.so.8"
RPM_SONAME_REQ_libksba = "ld-linux-aarch64.so.1 libc.so.6 libgpg-error.so.0"
RDEPENDS_libksba = "glibc libgpg-error"
RPM_SONAME_REQ_libksba-devel = "libksba.so.8"
RPROVIDES_libksba-devel = "libksba-dev (= 1.3.5)"
RDEPENDS_libksba-devel = "bash info libksba"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libksba-1.3.5-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libksba-devel-1.3.5-7.el8.aarch64.rpm \
          "

SRC_URI[libksba.sha256sum] = "268145276c48fbb98f90edc9a4379eb30ddc8a9a14d93f5970a7c89281ac7e14"
SRC_URI[libksba-devel.sha256sum] = "9485b25b2b4c789897e5249ad3a25b24bf7383c857ecc029024707dc94cddcfb"
