SUMMARY = "generated recipe based on hyphen-uk srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-uk = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-uk-0.20030903-16.el8.noarch.rpm \
          "

SRC_URI[hyphen-uk.sha256sum] = "9f42ad00bff56dd9411c0ac4153fb42c22001143dd68f7efb890cdf9c11409d4"
