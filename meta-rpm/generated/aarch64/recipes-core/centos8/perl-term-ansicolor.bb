SUMMARY = "generated recipe based on perl-Term-ANSIColor srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-ANSIColor = "perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Term-ANSIColor-4.06-396.el8.noarch.rpm \
          "

SRC_URI[perl-Term-ANSIColor.sha256sum] = "f4e3607f242bbca7ec2379822ca961860e6d9c276da51c6e2dfd17a29469ec78"
