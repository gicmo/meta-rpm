SUMMARY = "generated recipe based on mythes-it srpm"
DESCRIPTION = "Description"
LICENSE = "AGPL-3.0"
RPM_LICENSE = "AGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-it = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-it-2.0.9l-18.el8.noarch.rpm \
          "

SRC_URI[mythes-it.sha256sum] = "aa8f70e59221fc0b0944fea55e85f07701160aea688749208ad5bb96383284d8"
