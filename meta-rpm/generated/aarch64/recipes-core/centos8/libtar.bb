SUMMARY = "generated recipe based on libtar srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_libtar = "libtar.so.1"
RPM_SONAME_REQ_libtar = "ld-linux-aarch64.so.1 libc.so.6 libz.so.1"
RDEPENDS_libtar = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtar-1.2.20-15.el8.aarch64.rpm \
          "

SRC_URI[libtar.sha256sum] = "5e1fd5f60f0db8bf98f53cfc53de1b60743e6659d26a898f1fc1732d38248003"
