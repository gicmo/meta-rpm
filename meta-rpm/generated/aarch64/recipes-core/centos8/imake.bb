SUMMARY = "generated recipe based on imake srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_imake = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_imake = "bash glibc perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/imake-1.0.7-11.el8.aarch64.rpm \
          "

SRC_URI[imake.sha256sum] = "5283564fa117b8db19c69330ec11b125e4326f0990d645e4e86fb873d32ca860"
