SUMMARY = "generated recipe based on python-iniparse srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & Python-2.0"
RPM_LICENSE = "MIT and Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-iniparse = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-iniparse-0.4-31.el8.noarch.rpm \
          "

SRC_URI[python3-iniparse.sha256sum] = "5c0957decce3babef9864904be13190b0072aef720e9f4ca820bab6c02a20178"
