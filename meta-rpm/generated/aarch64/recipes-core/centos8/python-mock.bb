SUMMARY = "generated recipe based on python-mock srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-mock = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-mock-2.0.0-11.el8.noarch.rpm \
          "

SRC_URI[python3-mock.sha256sum] = "62e125a0c55cfa69ac8000d7fecab0574509663de326450f5c66bd36f7308b2c"
