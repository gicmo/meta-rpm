SUMMARY = "generated recipe based on perl-YAML-LibYAML srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-YAML-LibYAML = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-YAML-LibYAML = "glibc perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-YAML-LibYAML-0.70-1.el8.aarch64.rpm \
          "

SRC_URI[perl-YAML-LibYAML.sha256sum] = "ef4d4bee093a7a2993356cef1afdbea81f9753125e47d38b9463507d14da70a7"
