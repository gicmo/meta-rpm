SUMMARY = "generated recipe based on python-pyudev srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyudev = "platform-python python3-six systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-pyudev-0.21.0-7.el8.noarch.rpm \
          "

SRC_URI[python3-pyudev.sha256sum] = "aa0007192287faeaecc72d9fa48efdb3108c764d450dc8fea65474476da32c45"
