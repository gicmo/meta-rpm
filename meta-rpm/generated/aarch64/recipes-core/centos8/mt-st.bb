SUMMARY = "generated recipe based on mt-st srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mt-st = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mt-st = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mt-st-1.1-24.el8.aarch64.rpm \
          "

SRC_URI[mt-st.sha256sum] = "1a8d8ea22ea15837ab8525157ee0b577c6d00864390a7b749c5e2385bdf5acc4"
