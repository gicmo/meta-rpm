SUMMARY = "generated recipe based on perl-Sub-Uplevel srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Sub-Uplevel = "perl-Carp perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Sub-Uplevel-0.2800-4.el8.noarch.rpm \
          "

SRC_URI[perl-Sub-Uplevel.sha256sum] = "c10d1211ce3fc8a31fc124f962cef6631cc9f86ca6e1461381f4164d98a1061f"
