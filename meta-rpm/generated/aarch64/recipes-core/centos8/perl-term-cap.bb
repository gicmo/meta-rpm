SUMMARY = "generated recipe based on perl-Term-Cap srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-Cap = "ncurses perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Term-Cap-1.17-395.el8.noarch.rpm \
          "

SRC_URI[perl-Term-Cap.sha256sum] = "6bbb721dd2c411c85c75f7477b14c54c776d78ee9b93557615e919ef47577440"
