SUMMARY = "generated recipe based on krb5 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "e2fsprogs keyutils libselinux libverto pkgconfig-native"
RPM_SONAME_REQ_krb5-devel = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libgssapi_krb5.so.2 libgssrpc.so.4 libk5crypto.so.3 libkdb5.so.9 libkeyutils.so.1 libkrad.so.0 libkrb5.so.3 libkrb5support.so.0 libresolv.so.2 libselinux.so.1"
RPROVIDES_krb5-devel = "krb5-dev (= 1.17)"
RDEPENDS_krb5-devel = "bash glibc keyutils-libs keyutils-libs-devel krb5-libs libcom_err libcom_err-devel libkadm5 libselinux libselinux-devel libverto-devel openssl-libs pkgconf-pkg-config"
RPM_SONAME_PROV_krb5-libs = "libgssapi_krb5.so.2 libgssrpc.so.4 libk5crypto.so.3 libkdb5.so.9 libkrad.so.0 libkrb5.so.3 libkrb5support.so.0"
RPM_SONAME_REQ_krb5-libs = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libkeyutils.so.1 libresolv.so.2 libselinux.so.1 libssl.so.1.1 libverto.so.1"
RDEPENDS_krb5-libs = "bash coreutils crypto-policies gawk glibc grep keyutils-libs libcom_err libselinux libverto openssl-libs sed"
RPM_SONAME_PROV_libkadm5 = "libkadm5clnt_mit.so.11 libkadm5srv_mit.so.11"
RPM_SONAME_REQ_libkadm5 = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libgssapi_krb5.so.2 libgssrpc.so.4 libk5crypto.so.3 libkdb5.so.9 libkeyutils.so.1 libkrb5.so.3 libkrb5support.so.0 libresolv.so.2"
RDEPENDS_libkadm5 = "glibc keyutils-libs krb5-libs libcom_err openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/krb5-devel-1.17-18.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/krb5-libs-1.17-18.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libkadm5-1.17-18.el8.aarch64.rpm \
          "

SRC_URI[krb5-devel.sha256sum] = "0a424b5755df55fdf10a2cc1ee4d34cb1572f9ef64c8a0137bf602048ecf158d"
SRC_URI[krb5-libs.sha256sum] = "5a512c291feeb3fe3795821b3d0034a46017890e9826db6eda4f4c3c10fb708e"
SRC_URI[libkadm5.sha256sum] = "83b8bd7ca55a62788b67ad33bf17dcc266d50a067c3d654035225790e7436d40"
