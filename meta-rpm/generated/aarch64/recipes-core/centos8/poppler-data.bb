SUMMARY = "generated recipe based on poppler-data srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & GPL-2.0"
RPM_LICENSE = "BSD and GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/poppler-data-0.4.9-1.el8.noarch.rpm \
          "

SRC_URI[poppler-data.sha256sum] = "453ae84d0afbce380631ab4400ffa8fdcc56a5d0ba4001675fdb90de5fb65901"
