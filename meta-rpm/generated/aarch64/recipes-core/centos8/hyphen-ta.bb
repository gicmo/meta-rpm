SUMMARY = "generated recipe based on hyphen-ta srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ta = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-ta-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-ta.sha256sum] = "f0c71278e5e9bf76dfe71d880f1646a4a11e0ffeaae073cda6dba2e76a10df89"
