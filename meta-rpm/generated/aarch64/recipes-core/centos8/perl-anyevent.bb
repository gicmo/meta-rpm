SUMMARY = "generated recipe based on perl-AnyEvent srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-AnyEvent = "perl-Carp perl-Errno perl-Exporter perl-File-Temp perl-Net-SSLeay perl-Scalar-List-Utils perl-Socket perl-Time-HiRes perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-AnyEvent-7.14-6.el8.aarch64.rpm \
          "

SRC_URI[perl-AnyEvent.sha256sum] = "47d3e0675e9eb40fc607a84133122e5662167bebc901d54bdbe83f827bbf1e97"
