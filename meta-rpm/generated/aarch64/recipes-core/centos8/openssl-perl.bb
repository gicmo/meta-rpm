SUMMARY = "generated recipe based on openssl srpm"
DESCRIPTION = "Description"
LICENSE = "OpenSSL"
RPM_LICENSE = "OpenSSL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_openssl-perl = "openssl perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssl-perl-1.1.1c-15.el8.aarch64.rpm \
          "

SRC_URI[openssl-perl.sha256sum] = "24af4bbc8e4af1e5e7d0a3ae1a3ddc9f0e08e8a90a02936645f308cab2563b71"
