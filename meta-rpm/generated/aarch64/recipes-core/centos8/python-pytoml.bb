SUMMARY = "generated recipe based on python-pytoml srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pytoml = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pytoml-0.1.14-5.git7dea353.el8.noarch.rpm \
          "

SRC_URI[python3-pytoml.sha256sum] = "bc4787f3b225a535a79a1279ae87be45bfdfde1958c6a7b0f6e71ac32a480972"
