SUMMARY = "generated recipe based on perl-PerlIO-via-QuotedPrint srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-PerlIO-via-QuotedPrint = "perl-MIME-Base64 perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-PerlIO-via-QuotedPrint-0.08-395.el8.noarch.rpm \
          "

SRC_URI[perl-PerlIO-via-QuotedPrint.sha256sum] = "21b6e845423ade70758066740a3cdd946ef0529c870d3b7723ac830181afcce2"
