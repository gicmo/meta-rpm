SUMMARY = "generated recipe based on python-cryptography srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED | BSD"
RPM_LICENSE = "ASL 2.0 or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-cryptography = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1"
RDEPENDS_python3-cryptography = "glibc openssl-libs platform-python python3-asn1crypto python3-cffi python3-idna python3-libs python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-cryptography-2.3-3.el8.aarch64.rpm \
          "

SRC_URI[python3-cryptography.sha256sum] = "864c6fb310c1dff0968fd628e85020627343ce0fd48986d5be9e55cb9c3d28c0"
