SUMMARY = "generated recipe based on perl-Text-Glob srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-Glob = "perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Text-Glob-0.11-4.el8.noarch.rpm \
          "

SRC_URI[perl-Text-Glob.sha256sum] = "6bc31ceb18afe8d4e855c391029f8b2118b42d05d45de508b8c472dedbcce7ee"
