SUMMARY = "generated recipe based on python-rpm-generators srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-rpm-generators = "bash platform-python platform-python-setuptools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-rpm-generators-5-6.el8.noarch.rpm \
          "

SRC_URI[python3-rpm-generators.sha256sum] = "edacaa884fb6d7c427d6ca78beb30bd4c91821aee1cd0eba8442f705cfeae37d"
