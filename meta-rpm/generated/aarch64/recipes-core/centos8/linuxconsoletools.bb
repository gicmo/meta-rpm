SUMMARY = "generated recipe based on linuxconsoletools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native sdl"
RPM_SONAME_REQ_linuxconsoletools = "ld-linux-aarch64.so.1 libSDL-1.2.so.0 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_linuxconsoletools = "SDL bash gawk glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/linuxconsoletools-1.6.0-4.el8.aarch64.rpm \
          "

SRC_URI[linuxconsoletools.sha256sum] = "1f7ca88082105d8af04c111c9ddf1c15c8a153713a052aca0b37c7ee52d85a61"
