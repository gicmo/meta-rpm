SUMMARY = "generated recipe based on m17n-db srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_m17n-db = "gawk"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/m17n-db-1.8.0-3.el8.noarch.rpm \
          "

SRC_URI[m17n-db.sha256sum] = "5cd72f4e9a22b0d578f99d6c1e24c50d110e44aae69ccb347b902560874e8725"
