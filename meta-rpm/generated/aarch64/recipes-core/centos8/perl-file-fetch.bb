SUMMARY = "generated recipe based on perl-File-Fetch srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Fetch = "perl-Carp perl-File-Path perl-File-Temp perl-IPC-Cmd perl-Locale-Maketext-Simple perl-Module-Load-Conditional perl-Params-Check perl-PathTools perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-File-Fetch-0.56-2.el8.noarch.rpm \
          "

SRC_URI[perl-File-Fetch.sha256sum] = "4d1714f9155045bf295c9b1b5815881fcd0b3596cfb29541135c24206864233a"
