SUMMARY = "generated recipe based on malaga-suomi-voikko srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/malaga-suomi-voikko-1.19-5.el8.aarch64.rpm \
          "

SRC_URI[malaga-suomi-voikko.sha256sum] = "6f168d0670447b079acef74e604c5f5cc1ee0f571fa50ede7139cdfbb31ea85a"
