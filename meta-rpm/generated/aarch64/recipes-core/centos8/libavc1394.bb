SUMMARY = "generated recipe based on libavc1394 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libraw1394 pkgconfig-native"
RPM_SONAME_PROV_libavc1394 = "libavc1394.so.0 librom1394.so.0"
RPM_SONAME_REQ_libavc1394 = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libraw1394.so.11"
RDEPENDS_libavc1394 = "glibc libraw1394"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libavc1394-0.5.4-7.el8.aarch64.rpm \
          "

SRC_URI[libavc1394.sha256sum] = "08214cb23554191ea623645cb503ce2d5d82c413506adaca8f45f09f97a0069c"
