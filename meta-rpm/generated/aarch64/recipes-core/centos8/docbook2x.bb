SUMMARY = "generated recipe based on docbook2X srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_docbook2X = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_docbook2X = "bash glibc info libxslt openjade opensp perl-Exporter perl-Getopt-Long perl-IO perl-Text-Tabs+Wrap perl-XML-SAX perl-interpreter perl-libs texinfo"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/docbook2X-0.8.8-29.el8.aarch64.rpm \
          "

SRC_URI[docbook2X.sha256sum] = "cd1cc8c934e3d74c6b1ed12bbffe386c9aebc77f58436704abd24609a3542f4d"
