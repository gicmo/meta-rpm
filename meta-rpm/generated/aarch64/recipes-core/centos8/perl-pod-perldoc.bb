SUMMARY = "generated recipe based on perl-Pod-Perldoc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Perldoc = "groff-base perl-Carp perl-Encode perl-File-Temp perl-HTTP-Tiny perl-IO perl-PathTools perl-Pod-Simple perl-Text-ParseWords perl-interpreter perl-libs perl-parent perl-podlators"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Pod-Perldoc-3.28-396.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Perldoc.sha256sum] = "0225dc3999e3d7b1bb57186a2fc93c98bd1e4e08e062fb51c966e1f2a2c91bb4"
