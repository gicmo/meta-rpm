SUMMARY = "generated recipe based on pnm2ppa srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_pnm2ppa = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_pnm2ppa = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pnm2ppa-1.04-40.el8.aarch64.rpm \
          "

SRC_URI[pnm2ppa.sha256sum] = "5192f621c471b8ade6ee4bf110149182af9d2a6839178c6e7a9247b246488688"
