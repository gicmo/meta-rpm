SUMMARY = "generated recipe based on hunspell-pl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-2.0 | MPL-1.1 | CLOSED | CC-BY-SA-1.0"
RPM_LICENSE = "LGPLv2+ or GPL+ or MPLv1.1 or ASL 2.0 or CC-BY-SA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-pl = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-pl-0.20180707-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-pl.sha256sum] = "fec89c9d8e0285969eed1da6eeee3a276d31043065bebcdad550b89eb90334cb"
