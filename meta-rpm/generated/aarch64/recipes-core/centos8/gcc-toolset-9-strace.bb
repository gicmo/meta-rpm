SUMMARY = "generated recipe based on gcc-toolset-9-strace srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.1 & GPL-2.0"
RPM_LICENSE = "LGPL-2.1+ and GPL-2.0+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 pkgconfig-native xz zlib"
RPM_SONAME_REQ_gcc-toolset-9-strace = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 liblzma.so.5 librt.so.1 libz.so.1"
RDEPENDS_gcc-toolset-9-strace = "bash bzip2-libs gcc-toolset-9-runtime glibc xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-strace-5.1-6.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-strace.sha256sum] = "2712b118a9aae5141aa6a740b6929fe9c8d4a4737b0a4bd626f83349acc2631d"
