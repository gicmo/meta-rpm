SUMMARY = "generated recipe based on ethtool srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ethtool = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_ethtool = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ethtool-5.0-2.el8.aarch64.rpm \
          "

SRC_URI[ethtool.sha256sum] = "6f22032249e36b6cf05bb4346b2864e3e2c94aea0d872ed2eede0e8f0ebb908c"
