SUMMARY = "generated recipe based on perl-XML-Parser srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "expat perl pkgconfig-native"
RPM_SONAME_REQ_perl-XML-Parser = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-XML-Parser = "expat glibc perl-Carp perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-XML-Parser-2.44-11.el8.aarch64.rpm \
          "

SRC_URI[perl-XML-Parser.sha256sum] = "f198fcbe2a4ad7cabb3020dcf205b85ac5fbdf3ccf6839edfd96053bf948d838"
