SUMMARY = "generated recipe based on hunspell-ia srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ia = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ia-0.20050226-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-ia.sha256sum] = "8bf77f9d201ff75f5b8c92914eca4a16ea8b1e38995f128b96a796e4236fd863"
