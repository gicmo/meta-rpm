SUMMARY = "generated recipe based on watchdog srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libtirpc pkgconfig-native"
RPM_SONAME_REQ_watchdog = "ld-linux-aarch64.so.1 libc.so.6 libtirpc.so.3"
RDEPENDS_watchdog = "bash glibc libtirpc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/watchdog-5.15-1.el8.aarch64.rpm \
          "

SRC_URI[watchdog.sha256sum] = "3feac4eb5bd3562d67d85e5f414400df52c32a8c02434221f2425a9ac39e46a9"
