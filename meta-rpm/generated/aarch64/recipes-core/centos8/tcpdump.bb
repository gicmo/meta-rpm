SUMMARY = "generated recipe based on tcpdump srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "BSD with advertising"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libpcap openssl pkgconfig-native"
RPM_SONAME_REQ_tcpdump = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpcap.so.1"
RDEPENDS_tcpdump = "bash glibc libpcap openssl-libs shadow-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tcpdump-4.9.2-6.el8.aarch64.rpm \
          "

SRC_URI[tcpdump.sha256sum] = "d1b7aaaf39bf1d9173ca33e975b35f06276b31d4b9b779da48f13c38bf9da327"
