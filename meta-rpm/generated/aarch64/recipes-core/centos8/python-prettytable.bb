SUMMARY = "generated recipe based on python-prettytable srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-prettytable = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-prettytable-0.7.2-14.el8.noarch.rpm \
          "

SRC_URI[python3-prettytable.sha256sum] = "1b53c868277dec628058b31b1a8a8a2ccd8efe832ce9694805b5f82564136eb3"
