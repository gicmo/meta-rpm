SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-so = "locale-base-so-dj (= 2.28) locale-base-so-dj.utf8 (= 2.28) locale-base-so-et (= 2.28) locale-base-so-ke (= 2.28) locale-base-so-ke.utf8 (= 2.28) locale-base-so-so (= 2.28) locale-base-so-so.utf8 (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so-dj (= 2.28) virtual-locale-so-dj.utf8 (= 2.28) virtual-locale-so-et (= 2.28) virtual-locale-so-ke (= 2.28) virtual-locale-so-ke.utf8 (= 2.28) virtual-locale-so-so (= 2.28) virtual-locale-so-so.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-so = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-so-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-so.sha256sum] = "96968f7939a17036899b893c4cbfc9b72148331d7667a54db0b8094dda873ef5"
