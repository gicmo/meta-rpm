SUMMARY = "generated recipe based on sonatype-plugins-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sonatype-plugins-parent = "forge-parent java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sonatype-plugins-parent-8-12.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[sonatype-plugins-parent.sha256sum] = "ff7400f30e4e711315bc9e50ca28fdff23545a2568ec089bfdfbff1c3d6dd445"
