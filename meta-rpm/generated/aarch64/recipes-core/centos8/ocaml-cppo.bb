SUMMARY = "generated recipe based on ocaml-cppo srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ocaml-cppo = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_ocaml-cppo = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-cppo-1.6.4-4.el8.aarch64.rpm \
          "

SRC_URI[ocaml-cppo.sha256sum] = "ab5e25e41043d71af8ae2998c1be32a4ea0d3e210c2c140b50cfbc1bf9b13cbd"
