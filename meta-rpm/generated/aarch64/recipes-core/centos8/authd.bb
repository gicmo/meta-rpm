SUMMARY = "generated recipe based on authd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_authd = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1"
RDEPENDS_authd = "bash glibc openssl openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/authd-1.4.4-5.el8_0.1.aarch64.rpm \
          "

SRC_URI[authd.sha256sum] = "33023d6fee72093b6fea43d62c8daad21119fbf99b3aabf01c847f3671d637c6"
