SUMMARY = "generated recipe based on iso-codes srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_iso-codes = "xml-common"
RPROVIDES_iso-codes-devel = "iso-codes-dev (= 3.79)"
RDEPENDS_iso-codes-devel = "iso-codes pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/iso-codes-3.79-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/iso-codes-devel-3.79-2.el8.noarch.rpm \
          "

SRC_URI[iso-codes.sha256sum] = "f5a0a39b40f2af0b74ec47f6a5e00f7772ac8bd347c793b7deac84d3d8d7d47a"
SRC_URI[iso-codes-devel.sha256sum] = "615dc1fb42aba91d87214d62967f833889f22f9d77bff3ae9704702df44b2bbe"
