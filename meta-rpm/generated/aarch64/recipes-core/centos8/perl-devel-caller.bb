SUMMARY = "generated recipe based on perl-Devel-Caller srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-Caller = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-Caller = "glibc perl-Exporter perl-PadWalker perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Devel-Caller-2.06-15.el8.aarch64.rpm \
          "

SRC_URI[perl-Devel-Caller.sha256sum] = "0a4d160601080cc27fa36253957c61d37e98cc193f1bd128a6157ad750e6421c"
