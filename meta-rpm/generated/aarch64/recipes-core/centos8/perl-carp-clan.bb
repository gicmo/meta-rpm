SUMMARY = "generated recipe based on perl-Carp-Clan srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Carp-Clan = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Carp-Clan-6.06-6.el8.noarch.rpm \
          "

SRC_URI[perl-Carp-Clan.sha256sum] = "eb9074981bd361a4e1fd389c9bbde50155f07a48cb365b8502e73f42ab9d19a8"
