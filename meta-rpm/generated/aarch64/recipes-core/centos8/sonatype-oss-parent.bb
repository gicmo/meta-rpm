SUMMARY = "generated recipe based on sonatype-oss-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sonatype-oss-parent = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sonatype-oss-parent-7-14.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[sonatype-oss-parent.sha256sum] = "47db647744dc0051e8c35b8363bc8eb809dee3dbd1bbfd83dec7d753d49f3b16"
