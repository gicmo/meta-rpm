SUMMARY = "generated recipe based on perl-Devel-LexAlias srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-LexAlias = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-LexAlias = "glibc perl-Devel-Caller perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Devel-LexAlias-0.05-16.el8.aarch64.rpm \
          "

SRC_URI[perl-Devel-LexAlias.sha256sum] = "ab3ed847182b3f7d0321bba5692250bd699d731fbde5c09524ebc5168d277d6f"
