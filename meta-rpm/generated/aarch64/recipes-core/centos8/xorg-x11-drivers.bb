SUMMARY = "generated recipe based on xorg-x11-drivers srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_xorg-x11-drivers = "xorg-x11-drv-ati xorg-x11-drv-dummy xorg-x11-drv-evdev xorg-x11-drv-fbdev xorg-x11-drv-libinput xorg-x11-drv-nouveau xorg-x11-drv-v4l xorg-x11-drv-wacom xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drivers-7.7-22.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drivers.sha256sum] = "4485e808e7cca0d7d8520381ff0a8000f7fa5bdfd421817748fc56f22cda0aa2"
