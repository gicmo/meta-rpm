SUMMARY = "generated recipe based on perl-WWW-RobotRules srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-WWW-RobotRules = "perl-Carp perl-URI perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-WWW-RobotRules-6.02-18.el8.noarch.rpm \
          "

SRC_URI[perl-WWW-RobotRules.sha256sum] = "5e4cd40361c568a069726790f3936f3144d32615f65854016eb652fd86287718"
