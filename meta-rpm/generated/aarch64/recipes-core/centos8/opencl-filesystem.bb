SUMMARY = "generated recipe based on opencl-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/opencl-filesystem-1.0-6.el8.noarch.rpm \
          "

SRC_URI[opencl-filesystem.sha256sum] = "7fcf1728304abec17ea82bf8d807810fff411b527b9fc84a346e73fe4ac4e933"
