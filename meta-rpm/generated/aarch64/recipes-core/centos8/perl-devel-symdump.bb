SUMMARY = "generated recipe based on perl-Devel-Symdump srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Devel-Symdump = "perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Devel-Symdump-2.18-5.el8.noarch.rpm \
          "

SRC_URI[perl-Devel-Symdump.sha256sum] = "360a374ece2ff8da362873beecaa4174d47b03c668d5b61ec90ac725a6d2d680"
