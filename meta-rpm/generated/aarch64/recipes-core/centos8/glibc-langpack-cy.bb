SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-cy = "locale-base-cy-gb (= 2.28) locale-base-cy-gb.utf8 (= 2.28) virtual-locale-cy (= 2.28) virtual-locale-cy (= 2.28) virtual-locale-cy-gb (= 2.28) virtual-locale-cy-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-cy = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-cy-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-cy.sha256sum] = "3ff14b8da7b350903e2eaaf62f9f69d1716da5fbb73d8f930f20e1ea38220098"
