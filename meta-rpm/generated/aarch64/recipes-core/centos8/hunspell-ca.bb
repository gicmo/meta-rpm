SUMMARY = "generated recipe based on hunspell-ca srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ca = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ca-2.3-11.el8.noarch.rpm \
          "

SRC_URI[hunspell-ca.sha256sum] = "60752172611bf7ab3dabc7b91c21992b155016ca6b65ec2170e63b8703136252"
