SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-sv = "locale-base-sv-fi (= 2.28) locale-base-sv-fi.utf8 (= 2.28) locale-base-sv-fi@euro (= 2.28) locale-base-sv-se (= 2.28) locale-base-sv-se.iso885915 (= 2.28) locale-base-sv-se.utf8 (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv-fi (= 2.28) virtual-locale-sv-fi.utf8 (= 2.28) virtual-locale-sv-fi@euro (= 2.28) virtual-locale-sv-se (= 2.28) virtual-locale-sv-se.iso885915 (= 2.28) virtual-locale-sv-se.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-sv = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sv-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-sv.sha256sum] = "b74e5e08b24209dbdef3e304e4fea96c7585b6630e513e703a4ae14e2c02b8c2"
