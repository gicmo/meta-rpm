SUMMARY = "generated recipe based on perl-threads-shared srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-threads-shared = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-threads-shared = "glibc perl-Carp perl-Scalar-List-Utils perl-libs perl-threads"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-threads-shared-1.58-2.el8.aarch64.rpm \
          "

SRC_URI[perl-threads-shared.sha256sum] = "a6b6bcf4954aa87581ceadd5d3717e029efe14ce7fb918f442b78c9bb37d7c97"
