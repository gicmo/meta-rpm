SUMMARY = "generated recipe based on hyphen-kn srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-kn = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-kn-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-kn.sha256sum] = "baf9defcb7c44ddab9ff63d1cbd9e7b522d7656ad101e7d9be37a74b441ecf36"
