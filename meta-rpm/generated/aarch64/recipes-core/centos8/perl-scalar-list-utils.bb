SUMMARY = "generated recipe based on perl-Scalar-List-Utils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Scalar-List-Utils = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Scalar-List-Utils = "glibc perl-Carp perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Scalar-List-Utils-1.49-2.el8.aarch64.rpm \
          "

SRC_URI[perl-Scalar-List-Utils.sha256sum] = "a5d3afddd15296fa80bf47c18ac4bcb8caba6621998a98baf880726b21b232ad"
