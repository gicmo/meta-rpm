SUMMARY = "generated recipe based on perl-Devel-PPPort srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-PPPort = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-PPPort = "glibc perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Devel-PPPort-3.36-5.el8.aarch64.rpm \
          "

SRC_URI[perl-Devel-PPPort.sha256sum] = "0863c07e247b9e540c5b89257a9125ad2b3f5c0606fcaeab1c40d5a84974a1f7"
