SUMMARY = "generated recipe based on units srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native readline"
RPM_SONAME_REQ_units = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libreadline.so.7"
RDEPENDS_units = "bash glibc info platform-python readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/units-2.17-5.el8.aarch64.rpm \
          "

SRC_URI[units.sha256sum] = "afbdd1f40f6a7c53e8fac1c9cd531d1167d9242aed1ae9099c190e85fc8d9faa"
