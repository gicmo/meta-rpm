SUMMARY = "generated recipe based on spice-qxl-xddm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-qxl-xddm-0.1-24.el8.2.noarch.rpm \
          "

SRC_URI[spice-qxl-xddm.sha256sum] = "2c2e31893be82f7e50ea96ff943ee2b9b391327677d2ec38f3a8973bcab3ac5e"
