SUMMARY = "generated recipe based on rpm-mpi-hooks srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_rpm-mpi-hooks = "bash environment-modules filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rpm-mpi-hooks-5-4.el8.noarch.rpm \
          "

SRC_URI[rpm-mpi-hooks.sha256sum] = "4717be8d9be931dc98ced8b0e86e286edc95fdd3826908439c9d6d76e2ff53c7"
