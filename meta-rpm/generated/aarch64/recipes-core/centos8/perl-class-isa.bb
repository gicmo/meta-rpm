SUMMARY = "generated recipe based on perl-Class-ISA srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-ISA = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Class-ISA-0.36-1022.el8.noarch.rpm \
          "

SRC_URI[perl-Class-ISA.sha256sum] = "05b990cae721cfe078bd0434e4a4dba5a219c25d6f87b43f1d006bbb351eb51c"
