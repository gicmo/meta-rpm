SUMMARY = "generated recipe based on python-dns srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-dns = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-dns-1.15.0-10.el8.noarch.rpm \
          "

SRC_URI[python3-dns.sha256sum] = "b248646dc953587300a2134fc489821a5b9b1d299e8b7dea382c78b18a9ffe51"
