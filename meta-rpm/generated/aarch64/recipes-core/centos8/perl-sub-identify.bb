SUMMARY = "generated recipe based on perl-Sub-Identify srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sub-Identify = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sub-Identify = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Sub-Identify-0.14-6.el8.aarch64.rpm \
          "

SRC_URI[perl-Sub-Identify.sha256sum] = "fe4dfe4af79b909c6892a779803f3e9f73b092f467e56919524f1516b0524aa3"
