SUMMARY = "generated recipe based on perl-Data-Dumper srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Data-Dumper = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Data-Dumper = "glibc perl-Carp perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Data-Dumper-2.167-399.el8.aarch64.rpm \
          "

SRC_URI[perl-Data-Dumper.sha256sum] = "24a7297b8f6db905749311728b2afee907149000b4502af8b86930f31a07bda0"
