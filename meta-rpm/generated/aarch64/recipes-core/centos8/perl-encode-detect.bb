SUMMARY = "generated recipe based on perl-Encode-Detect srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-1.1 | GPL-2.0 | LGPL-2.0"
RPM_LICENSE = "MPLv1.1 or GPLv2+ or LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc perl pkgconfig-native"
RPM_SONAME_REQ_perl-Encode-Detect = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libperl.so.5.26 libpthread.so.0 libstdc++.so.6"
RDEPENDS_perl-Encode-Detect = "glibc libgcc libstdc++ perl-Encode perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Encode-Detect-1.01-28.el8.aarch64.rpm \
          "

SRC_URI[perl-Encode-Detect.sha256sum] = "89b593cc7b1899c567ebe1fb758b993acc85b993f7701499eafaf3b38f156fab"
