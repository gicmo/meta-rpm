SUMMARY = "generated recipe based on hyphen-or srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-or = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-or-0.7.0-12.el8.noarch.rpm \
          "

SRC_URI[hyphen-or.sha256sum] = "d130127f547353a59f4086192e81cb9b780cd4de89069088c13071a30b5843f2"
