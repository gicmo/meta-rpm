SUMMARY = "generated recipe based on color-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_color-filesystem = "filesystem rpm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/color-filesystem-1-20.el8.noarch.rpm \
          "

SRC_URI[color-filesystem.sha256sum] = "8bff7bd153ee903849143db9b4cb011268250e287c0aa0603a4c1e0f195592b8"
