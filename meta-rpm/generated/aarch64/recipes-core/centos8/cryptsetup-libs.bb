SUMMARY = "generated recipe based on cryptsetup srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "device-mapper-libs json-c libblkid libuuid openssl pkgconfig-native"
RPM_SONAME_PROV_cryptsetup-libs = "libcryptsetup.so.12"
RPM_SONAME_REQ_cryptsetup-libs = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcrypto.so.1.1 libdevmapper.so.1.02 libjson-c.so.4 libpthread.so.0 libssl.so.1.1 libuuid.so.1"
RDEPENDS_cryptsetup-libs = "device-mapper-libs glibc json-c libblkid libuuid openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cryptsetup-libs-2.2.2-1.el8.aarch64.rpm \
          "

SRC_URI[cryptsetup-libs.sha256sum] = "f713b18f5057d9114659a3eda244240efad15da85a812a668fc33ebe409578cb"
