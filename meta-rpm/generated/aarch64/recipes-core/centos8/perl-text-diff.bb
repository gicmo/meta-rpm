SUMMARY = "generated recipe based on perl-Text-Diff srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & (GPL-2.0 | Artistic-1.0) & MIT"
RPM_LICENSE = "(GPL+ or Artistic) and (GPLv2+ or Artistic) and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-Diff = "perl-Algorithm-Diff perl-Carp perl-Exporter perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Text-Diff-1.45-2.el8.noarch.rpm \
          "

SRC_URI[perl-Text-Diff.sha256sum] = "15b87e079458628fdc3148d21c80e4e72f59ffc244467d34a3260dcb2c6863d0"
