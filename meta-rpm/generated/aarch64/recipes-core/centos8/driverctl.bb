SUMMARY = "generated recipe based on driverctl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_driverctl = "bash coreutils systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/driverctl-0.111-1.el8.noarch.rpm \
          "

SRC_URI[driverctl.sha256sum] = "523a09f4ee676dc83b3fceb7b9b4c26f3f2d52617cec9ff75a05773efd9bc072"
