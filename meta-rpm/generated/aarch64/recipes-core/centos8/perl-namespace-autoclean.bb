SUMMARY = "generated recipe based on perl-namespace-autoclean srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-namespace-autoclean = "perl-B-Hooks-EndOfScope perl-Scalar-List-Utils perl-Sub-Identify perl-libs perl-namespace-clean"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-namespace-autoclean-0.28-10.el8.noarch.rpm \
          "

SRC_URI[perl-namespace-autoclean.sha256sum] = "f9d59950eb08b1fd3abdd87c1a07e68c0ef62cc2bf0c88054f2d01ee33175697"
