SUMMARY = "generated recipe based on nvmetcli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_nvmetcli = "bash platform-python python3-configshell python3-kmod systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nvmetcli-0.6-2.el8.noarch.rpm \
          "

SRC_URI[nvmetcli.sha256sum] = "b1b7fb0d5f0093b7c6027245b3655779e206e953f51b6d1ddbb3c16c73c49d55"
