This is a sample container to use meta-rpm in a container.

Build it using:

```
$ podman build -t meta-rpm .
```

Alternatively, you can pull a pre-build version like so:

```
$ podman pull docker.io/alexl/meta-rpm
```

Then run it like:

```
$ podman run -t -i --device=/dev/fuse:/dev/fuse:rwm -v buildvolume:/data meta-rpm
```

This will create a new volume called `buildvolume` if it didn't exist yet, and
start a container with it in /data. If the volume is empty it will tell
you to run the command `initial-setup` which will set up the volume and enter
a build environment. If the volume was previously set up then it will
automatically enter the yocto build environment.

Once you are in the build environment you can run bitbake to trigger
builds like in a regular yocto setup.

For convenience, any arguments passed to the container run comman will
be run as a shell command inside the build env. For instance you can
directly start a build like so:

```
$ podman run -t -i --device=/dev/fuse:/dev/fuse:rwm -v buildvolume:/data meta-rpm bitbake centos8-test-image
```

Note that the volume created for the build data (named `buildvolume`
in above examples) can get rather big, so make sure you delete it with
the `podman volume` commands when you no longer need it.
