TOPDIR=.
BINDIR=$(TOPDIR)/bin
GENDIR=$(TOPDIR)/meta-rpm/generated

all: centos8-x86_64 centos8-aarch64

centos8-x86_64:
	rm -rf $(GENDIR)/x86_64/recipes-core/centos8/*
	$(BINDIR)/generate-recipes.py \
	   --mapping $(TOPDIR)/meta-rpm/recipes-core/centos8/centos8-mapping.yaml \
	   --dnfdir $(TOPDIR)/meta-rpm/recipes-core/centos8/dnf \
	   --destdir $(GENDIR)/x86_64/recipes-core/centos8/ \
	   --licenses $(TOPDIR)/meta-rpm/recipes-core/centos8/license_dict.json \
	   --arch x86_64
	ln -s `realpath --relative-to="$(GENDIR)/x86_64/recipes-core/centos8/" $(TOPDIR)/meta-rpm/recipes-core/centos8/files` $(GENDIR)/x86_64/recipes-core/centos8/files

centos8-aarch64:
	rm -rf $(GENDIR)/aarch64/recipes-core/centos8/*
	$(BINDIR)/generate-recipes.py \
	   --mapping $(TOPDIR)/meta-rpm/recipes-core/centos8/centos8-mapping.yaml \
	   --dnfdir $(TOPDIR)/meta-rpm/recipes-core/centos8/dnf \
	   --destdir $(GENDIR)/aarch64/recipes-core/centos8/ \
	   --licenses $(TOPDIR)/meta-rpm/recipes-core/centos8/license_dict.json \
	   --arch aarch64
	   ln -s `realpath --relative-to="$(GENDIR)/aarch64/recipes-core/centos8/" $(TOPDIR)/meta-rpm/recipes-core/centos8/files` $(GENDIR)/aarch64/recipes-core/centos8/files

